package cn.com.mypm.testCycleMgr.dto;

import cn.com.mypm.framework.transmission.dto.BaseDto;
import cn.com.mypm.object.TestCycleTask;

public class TestCycleDto extends BaseDto {

	private TestCycleTask testCycleTask;

	public TestCycleTask getTestCycleTask() {
		return testCycleTask;
	}

	public void setTestCycleTask(TestCycleTask testCycleTask) {
		this.testCycleTask = testCycleTask;
	}
}
