package cn.com.mypm.object;

import java.util.Date;

import cn.com.mypm.framework.transmission.JsonInterface;

public abstract  class TypeDefine implements JsonInterface {

	private Long typeId;
	private String compId;
	private String typeName;
	private String remark;
	private Integer isDefault;
	private String subName;
	private String status;
	private Date updDate;

	public TypeDefine() {

	}

	public TypeDefine(Long typeId, String typeName) {
		this.typeId = typeId;
		this.typeName = typeName;
	}
	public TypeDefine(Long typeId, String typeName,Integer isDefault,String remark,String status) {
		this.typeId = typeId;
		this.typeName = typeName;
		this.isDefault = isDefault;
		this.remark = remark;
		this.status = status;
		this.setSubName();
	}
	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;	
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public String getSubName() {
		return subName;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getUpdDate() {
		return updDate;
	}

	public void setUpdDate(Date upDate) {
		this.updDate = upDate;
	}
	
	public void setSubName(){
		
		if(getClass().getSimpleName().equals("BugFreq")){
			this.subName = "BUG频率";
		}else if(this.getClass().getSimpleName().equals("BugType")){
			this.subName = "BUG类型";
 		}else if(this.getClass().getSimpleName().equals("BugGrade")){
 			this.subName = "BUG等级";
		}else if(this.getClass().getSimpleName().equals("BugOpotunity")){
			this.subName = "BUG发现时机";
		}else if(this.getClass().getSimpleName().equals("BugPri")){
			this.subName = "BUG优先级";
		}else if(this.getClass().getSimpleName().equals("BugSource")){
			this.subName = "BUG来源";
		}else if(this.getClass().getSimpleName().equals("CasePri")){
			this.subName = "用例优先级";
		}else if(this.getClass().getSimpleName().equals("CaseType")){
			this.subName = "用例类型";
		}else if(this.getClass().getSimpleName().equals("GeneCause")){
			this.subName = "BUG测试时机";
		}else if(this.getClass().getSimpleName().equals("OccurPlant")){
			this.subName = "BUG发生平台";
		}else if(this.getClass().getSimpleName().equals("ImportPhase")){
			this.subName = "BUG引入原因";
		}		
	}
	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String toStrList() {
		StringBuffer sbf = new StringBuffer();
		sbf.append("{");
		sbf.append("id:'");
		sbf.append(getTypeId().toString());
		sbf.append("',data: [0,'','");
		sbf.append(subName);
		sbf.append("','");
		sbf.append(typeName);
		sbf.append("','");
		sbf.append("1".equals(status) ? "启用" : "停用");
		sbf.append("','");
		sbf.append(remark == null ? "" : remark);
		sbf.append("','");
		sbf.append(isDefault);
		sbf.append("'");
		sbf.append("]");
		sbf.append("}");
		return sbf.toString();
	}

	public String toStrUpdateInit() {
		StringBuffer sbf = new StringBuffer();
		setSubName();
		sbf.append("typeId=");
		sbf.append(getTypeId().toString());
		sbf.append("^");
		sbf.append("subName=").append(subName);
		sbf.append("^");
		sbf.append("typeName=").append(typeName);
		sbf.append("^");
		sbf.append("remark=").append(remark == null ? "" : remark);
		sbf.append("^");
		sbf.append("status=").append(status);
		sbf.append("^");
		sbf.append("isDefault=").append(isDefault);
		sbf.append("^");
		sbf.append("initSubName=").append(subName);
		return sbf.toString();
	}

	public String toStrUpdateRest() {
		StringBuffer sbf = new StringBuffer();
		sbf.append(getTypeId().toString());
		sbf.append("^");
		sbf.append("0,,");
		sbf.append(subName);
		sbf.append(",");
		sbf.append(typeName);
		sbf.append(",");
		sbf.append("1".equals(status) ? "启用" : "停用");
		sbf.append(",");
		sbf.append(remark == null ? "" : remark);
		sbf.append(",");
		sbf.append(isDefault);
		return sbf.toString();
	}

	public void toString(StringBuffer sbf) {
		sbf.append("{");
		sbf.append("id:'");
		sbf.append(getTypeId().toString());
		sbf.append("',data: [0,'','");
		sbf.append(subName);
		sbf.append("','");
		sbf.append(typeName);
		sbf.append("','");
		sbf.append("1".equals(status) ? "启用" : "停用");
		sbf.append("','");
		sbf.append(remark == null ? "" : remark);
		sbf.append("','");
		sbf.append(isDefault);
		sbf.append("'");
		sbf.append("]");
		sbf.append("}");
	}


}
