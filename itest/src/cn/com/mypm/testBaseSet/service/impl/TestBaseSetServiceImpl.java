
package cn.com.mypm.testBaseSet.service.impl;

import java.util.Date;

import cn.com.mypm.common.SecurityContextHolderHelp;
import cn.com.mypm.framework.app.services.BaseServiceImpl;
import cn.com.mypm.object.TypeDefine;
import cn.com.mypm.testBaseSet.dto.TestBaseSetDto;
import cn.com.mypm.testBaseSet.service.TestBaseSetService;

public class TestBaseSetServiceImpl extends BaseServiceImpl implements TestBaseSetService {

	public TypeDefine chgTypeDefine(TestBaseSetDto dto){
		String hql = "update TypeDefine set status = ? ,updDate = ? where typeId=?";
		this.executeUpdateByHql(hql, new Object[]{"3",new Date(),dto.getTestBaseSet().getTypeId()});
		TypeDefine td = dto.getTestBaseSet().copy2TypeDefine();
		td.setCompId(SecurityContextHolderHelp.getCompanyId());
		td.setUpdDate(new Date());
		synchronized (this){
			this.add(td);
		}
		return td;
	}
	

}
