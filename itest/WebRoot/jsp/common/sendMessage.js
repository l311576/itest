		var msgDetalW_ch, msgW_ch, objectType, objectId;
		function addMsg(msgCreateId, type, id){
			msgW_ch = initWch(msgW_ch,  "addMsgDiv", false, 380, 137, "", "发送反馈消息");
			formReset('addMsgF', 'msgRemark');
			$("msgText").innerHTML = "";
			$("msgCreateId").value = msgCreateId;
			objectType = type;objectId = id;
			if(type == "r"){
				$("subjectMsg").value = "\u5de5\u4f5c\u62a5\u544a\u53cd\u9988\u4fe1\u606f";
			}else if(type == "p"){
				$("subjectMsg").value = "\u5de5\u65f6\u5ba1\u6279\u53cd\u9988\u4fe1\u606f";
			}
			
		}
		
		function sendMsg(){
			if(isWhitespace($('msgRemark').value)){
				$("msgText").innerHTML = "请填写反馈消息的内容";
				return;
			}
			var url = conextPath + "/report/reportAction!sendReportMsg.action";
			if(typeof objectId != "undefined"){
				url = url + "?reportDto.messageType=" + objectType + "&reportDto.report.id=" + objectId;
			}
			var result = dhtmlxAjax.postSync(url, "addMsgF").xmlDoc.responseText;
			if(result != "failed" && result != ""){
				if(result == "noUsers"){
					$("msgText").innerHTML = "没有消息接收人";
					return;
				}
				msgW_ch.hide();
				$("msgCreateId").value = "";
				showMessage(false, "发送反馈信息", "发送反馈信息成功");
			}else{
				$("msgText").innerHTML = "发送反馈信息没有成功";
			}
		}

		function viewMsgDetl(messageId){
			var url =conextPath+"/msgManager/commonMsgAction!viewDetal.action?dto.isAjax=true&dto.broMsg.logicId="+messageId;
			url+="&dto.isView=1";
			var ajaxRest = postSub(url,"");
			if(ajaxRest=="failed"){
				showMessage(false, "反馈信息", "不能加载反馈信息");
				return;
			}else if(ajaxRest.indexOf("failed^")>=0){
				showMessage(false, "反馈信息", ajaxRest.split("^")[1]);
				return;			
			}
			setMsgUpInfo(ajaxRest);
			msgDetalW_ch=initWch(msgDetalW_ch, "msgDetalDiv", true,660, 330);
			msgDetalW_ch.setText("消息明细");
			fixCss('msgDetalTab');
			msgDetalW_ch.bringToTop();
			msgDetalW_ch.setModal(true);
			return;
		}
		
		function setMsgUpInfo(updInfo){
			$("msgType").disabled = true;
			var updInfos = updInfo.split("^");
			for(var i=0; i < updInfos.length; i++){
				var currInfo = updInfos[i].split("=");
				if(currInfo[1] != "null"){
					var valueStr = "";
					currInfo[1] =recovJsonKeyWord(currInfo[1]);
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
					try{eval(valueStr);}catch(e){}
					if(currInfo[0]=="content"){
						$("msgContentDiv").innerHTML=currInfo[1];
					}else if(currInfo[0]=="mailFlg"&&currInfo[1]=="true"){
						$("mailFlg").checked=true;
					}else if(currInfo[0]=="mailFlg"&&currInfo[1]!="true"){
						$("mailFlg").checked=false;
					}else if(currInfo[0]=="msgType"&&currInfo[1]=="1"){
						$("recpiUserIdTr").style.display="";
					}else if(currInfo[0]=="msgType"&&currInfo[1]!="1"){
						$("recpiUserIdTr").style.display="none";
					}
				}
			}
		}