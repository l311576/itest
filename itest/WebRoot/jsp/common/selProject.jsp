<%@ page contentType="text/html; charset=UTF-8"%>
	<div id="projectSelectDiv" style="border:0px;display:none;">
		<div class="gridbox_light" >
			<div class="objbox">
				<form  method="post" id="selProjectF" name="selProjectF"  action="">
					<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr class="ev_mypm">
							<td class="leftM_center">项目状态:</td>
							<td class="dataM_left" style="border-right:0px;" colspan="2">
								<input type="radio" name="projectDto.project.status" id="status1" checked="checked" value="1" onchange="findProjects();"/><label for="status1">进行中</label>
								<input type="radio" name="projectDto.project.status" id="status0" value="0" onchange="findProjects();"/><label for="status0">未开始</label>		
								<input type="radio" name="projectDto.project.status" id="status2" value="2" onchange="findProjects();"/><label for="status2">已完成</label>
								<input type="radio" name="projectDto.project.status" id="status_1" value="-1" onchange="findProjects();"/><label for="status_1">所有</label>
							</td>
						</tr>
						<tr class="odd_mypm">
							<td class="leftM_center">项目名称:</td>
							<td class="dataM_left" style="border-right:0px;"><input type="text" id="prjectName" name="projectDto.project.name" class="text_c" style="width:180px;" /></td>
							<td class="leftM_center" style="border-left:0px;">
							<img src="<%=request.getContextPath()%>/jsp/common/images/search.gif" onclick="findProjects();" title="查询" style="cursor:pointer;" />&nbsp;&nbsp;
							<img src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formReset('selProjectF', 'prjectName');$('projectSelectText').innerHTML = '';" title="重置" style="cursor:pointer;" />
							</td>
						</tr>
						<tr class="ev_mypm"><td colspan="3" style="border-right:0px;"><div id="projectSelectText" class="waring">&nbsp;</div></td></tr>
					</table>
				</form>
			</div>
		</div>
		<div id="selProjectDiv" style="margin-top:-13px;"></div>
	</div>
<script type="text/javascript">
	var isFinishArraySel = new Array("未开始","滞后","正常进行","提前开始","提前结束","正常结束","延期结束","人为终止","延期");
	var selProGrid, selProW_ch, projectId, projectName;
    function doOnCheck_selPro(rowId,cellInd,state){
    	if(state){
    		var name = selProGrid.cells(rowId, 3).getValue() + "(" + selProGrid.cells(rowId, 2).getValue() + ")";
    		excuteSelPro(rowId, name);
    	}
		return true;
	}
	
	function initRow(){
		var allItems = selProGrid.getAllItemIds();
		if(allItems == "")return;
		var items = allItems.split(',');
		var status;
		for(var i = 0; i < items.length; i++){
			status = selProGrid.cells(items[i],5).getValue();
			selProGrid.cells(items[i],5).setValue(isFinishArraySel[status]);
		}
	}
    
    function findProjects(){
    	selProW_ch = initW_ch(selProW_ch, "projectSelectDiv", false, 465, 315, "", "选择项目");
    	if(typeof selProGrid == "undefined"){
		    selProGrid = new dhtmlXGridObject('selProjectDiv');
			selProGrid.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
		    selProGrid.setHeader("&nbsp;,序号,版本编号,项目名称,项目负责人,项目状态");
		    selProGrid.setInitWidths("40,40,70,*,110,80");
		    selProGrid.setColAlign("center,center,left,left,left,center");
		    selProGrid.setColTypes("ra,ro,ro,ro,ro,ro");
		    selProGrid.setColSorting("int,int,str,str,str,str");
		    selProGrid.attachEvent("onCheckbox",doOnCheck_selPro);
			selProGrid.enableAutoHeight(true, 200);
		    selProGrid.init();
		    selProGrid.enableRowsHover(true, "red");
		    selProGrid.enableTooltips("false,true,true,true,true,true");
		    selProGrid.setSkin("light");
    	}
    	var result = dhtmlxAjax.postSync(conextPath + "/project/projectAction!findSelProject.action", "selProjectF").xmlDoc.responseText;
		selProGrid.clearAll();
		if(result != "failed" && result != ""){
			selProGrid.parse(eval("(" + result +")"), "json");
			initRow();
			$('projectSelectText').innerHTML = '';
		}else{
			$('projectSelectText').innerHTML = '没有查询到项目';
		}
		resetWinHeight(selProW_ch, "projectSelectDiv", 465, 315);
    }
    
    function selProject(proId, proName){
    	projectId = proId;
    	projectName = proName;
    	findProjects();
    	try{var rowId = $(proId).value;if(rowId != ""){selProGrid.setSelectedRow(rowId);selProGrid.cells(rowId,0).setValue(1);}}catch(e){}
    }
    
    function excuteSelPro(id, name){
    	$(projectId).value = id;
    	$(projectName).value = name;
    	selProW_ch.hide();
    }
    
    /**  调用示例：selProject(隐藏域, 显示域);
    <input type="text" id="prjectName1" name="projectDto.project.name1" onclick="selProject('prjectName2', 'prjectName1');"/>
	<input type="hidden" id="prjectName2" name="projectDto.project.name2" />
	**/
</script>