	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//最后一列为锁定人
    pmGrid.setHeader("&nbsp;,序号,文件名,上次更改,发布人,大小(KB),&nbsp;");
    if(typeof parent.currDocField=="undefined"||parent.currDocField==""){
		pmGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
		pmGrid.setInitWidths("0,40,*,100,100,80,0");
	}else{
		 pmGrid.setInitWidths("40,40,*,100,100,80,0");
		 pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro");;
	}
   
    pmGrid.setColAlign("center,center,left,left,left,center,left");
    pmGrid.setColSorting("int,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    
	var rootId = "",operState="0";
	var mypmTree=new dhtmlXTreeObject('treeBox',"100%","100%",0); 
	mypmTree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	mypmTree.enableTreeLines(true);
	mypmTree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	mypmTree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	mypmTree.setStdImages("book.gif","books_open.gif","books_close.gif");
	//parent.treeW_ch.setText("文件浏览器");
	mypmTree.enableHighlighting(1);
	var nodeData = $("nodeDataStr").value.split("$");
	var mypmTreeArr = nodeData[0].split(";");
	var myDirs = nodeData[1].split(",");
	var topNode = "";
	for(var i=0 ;i<mypmTreeArr.length;i++){
		var nodeInfo = mypmTreeArr[i].split(",");
		mypmTree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
		if(nodeInfo[3]=="0"&& i>0){
			mypmTree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
			mypmTree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,"");
			mypmTree.setUserData(nodeInfo[1]+"null","canAcc","0");
			mypmTree.closeItem(nodeInfo[1]);
		}
		if(i==0)
			rootId = nodeInfo[1];
		if(nodeInfo[4]==1&&isCanAcess(nodeInfo[1])){
			mypmTree.setItemColor(nodeInfo[1],'#ff0000','#ff0000');
			mypmTree.setUserData(nodeInfo[1],"canAcc","1");
		}else if(!isCanAcess(nodeInfo[1])){
			mypmTree.setItemColor(nodeInfo[1],'#BFC0BF','#BFC0BF');
			mypmTree.setUserData(nodeInfo[1],"canAcc","0");
		}else{
			mypmTree.setUserData(nodeInfo[1],"canAcc","1");
		}
	}
	mypmTree.setOnClickHandler(onclickHdl);
	mypmTree.setOnOpenEndHandler(onopenEndHdl);
	mypmTree.selectItem(rootId,true);
	//loadDocs(rootId);
	if($("parentNodeId").value!=""){
		mypmTree.selectItem($("parentNodeId").value,true);
		loadDocs($("parentNodeId").value);
	}
	function onclickHdl(id){
		var dbClick = id== $("currNodeId").value;
		if(dbClick){ //双击时，会引发两次单击，所以第二次只接返回
			return true;
		}
		$("currNodeId").value = id;
		formReset('findForm');
		$('publisherId').value='';
		var canAcc = mypmTree.getUserData(id,"canAcc");
		if(canAcc!="0"){
			loadDocs(id);
		}
		return;
	}
	function loadDocs(folderId){
		var url = conextPath+"/doc/docCommonAction!loadDocsInFolder.action?dto.parentId="+folderId;
		var ajaxResut = postSub(url, "");
		var userJson = ajaxResut.split("$");
		if(ajaxResut!="failed"){
			//try{
	   			pmGrid.clearAll();
	   			setPageNoSizeCount(userJson[0]);
	   		//}catch(err){}
	   		if(userJson[1]!=""){
	   			colTypeReset();
	   			pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   			loadLink();
	   			setRowNum(pmGrid);
	   			markRowColor();
	   		}
		}else if(ajaxResut=="failed"){
			hintMsg("加载目录文件出错");
		}		
	}
	function appendNode(treeArr,isOpening){
		var topNode = "";
		for(var i=0 ;i<treeArr.length;i++){
			var nodeInfo = treeArr[i].split(",");
			//当展开后，再增加时，己插入的不再插入
			if(mypmTree.getLevel(nodeInfo[1])!=0){
				continue;
			}
			topNode = nodeInfo[0];
			if(i==0){
				var blankId = mypmTree.getUserData(nodeInfo[0],"blankCh");
				var haveBlank = typeof blankId=="undefined" ;
				if(!haveBlank){
					mypmTree.deleteItem(blankId,false);
					mypmTree.setUserData(topNode,"blankCh");
				}
			}
			mypmTree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
			if(nodeInfo[4]==1&&isCanAcess(nodeInfo[1])){
				mypmTree.setItemColor(nodeInfo[1],'#ff0000','#ff0000');
				mypmTree.setUserData(nodeInfo[1],"canAcc","1");
			}else if(!isCanAcess(nodeInfo[1])){
				mypmTree.setUserData(nodeInfo[1],"canAcc","0");
				mypmTree.setItemColor(nodeInfo[1],'#BFC0BF','#BFC0BF');
			}else{
				mypmTree.setUserData(nodeInfo[1],"canAcc","1");
			}
			if(nodeInfo[3]=="0"){
				mypmTree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
				mypmTree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
				mypmTree.setUserData(nodeInfo[1]+"null","canAcc","0");
				mypmTree.closeItem(nodeInfo[1]);
			}
		}
		if(typeof isOpening!="undefined" ){
			mypmTree.closeItem(topNode);
		}
		return;
	}
	
	function loadChildren(itemId,isOpening){
		var url = conextPath+"/doc/docCommonAction!loadDirTree.action?dto.parentId="+itemId;
		var ajaxResut =  postSub(url,"");
		if(ajaxResut!="failed"&&ajaxResut!="$"){
			var nodeData = ajaxResut.split("$");
			myDirs = nodeData[1].split(",");			
			var treeArr = nodeData[0].split(";");
			appendNode(treeArr,isOpening);
		}else if(ajaxResut=="$"){
			mypmTree.deleteChildItems(itemId);
		}	
		return;	
	}
	function isCanAcess(mId){
		for(var i=0 ;i<myDirs.length;i++){
			if(myDirs[i]==mId){
				return true;
			}
		}
		return false;
	}
	function onopenEndHdl(id,mode){
		if(mode<0){
			return ;
		}
		var blankId = mypmTree.getUserData(id,"blankCh");
		var haveBlank = typeof blankId=="undefined" ;
		if(!haveBlank){
			loadChildren(id);
		}
	}
	
	function closeMe(){
		parent.pmDocSelW_ch.setModal(false);
		parent.pmDocSelW_ch.hide();	
		if(typeof(parent.itemMana_ch)!="undefined"){
			parent.itemMana_ch.setModal(false);
			parent.itemMana_ch.hide();
		}
	}
	function reFreshNode(){
		mypmTree.deleteChildItems($("currNodeId").value);
		loadChildren($("currNodeId").value);
	}
	
	pmBar.attachEvent("onClick", function(id){
		if(id =="find"){
			findInit();
		}else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
			}
	});
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});

    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		if(setSeledFile()){
			pmGrid.cells(rowId, 0).setValue(false);
			return true;
		}
		return false;
	}
    function doOnSelect(rowId,index){
		//pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	}
	function getTttle(rowId,colIn){
		return pmGrid.cells(rowId,colIn).getValue();
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,2).cell.innerHTML="<a href='javascript:openDoc("+pmGrid.getRowId(i)+")' title=\"查看\">"+getTttle2(i,2)+"</a>";	
		}
		sw2Link();
	}
	function colTypeReset(){
		pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ra,ro,link,ro,ro,ro,ro");
	}	

	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+"/doc/docCommonAction!loadDocsInFolder.action?dto.parentId="+$("currNodeId").value +"&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		loadLink();
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
	   		markRowColor();
   		}

	}	
	
	function findInit(){
		fW_ch = initW_ch(fW_ch, "findDiv", true, 440, 120);
		fW_ch.setText("查询");
		$("cUMTxtf").innerHTML="";
		fW_ch.show();
		fW_ch.bringToTop();
	}
	function findExe(){
		var url = conextPath+"/doc/docCommonAction!loadDocsInFolder.action?dto.parentId="+$("currNodeId").value;
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[0]=="failed"){
			hintMsg("查询发重错误");
			return;
		}
		if(userJson[1] == ""){
			pmGrid.clearAll();
			setPageNoSizeCount(userJson[0]);
			$("cUMTxtf").innerHTML="没查到相关记录";
		}else{
	   		pmGrid.clearAll();
	   		colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		loadLink();
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
	   		markRowColor();
	   		fW_ch.setModal(false);
	   		fW_ch.hide();
   		}
	}
	function markRowColor(){
	 	var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){	
			if(pmGrid.cells(items[i],6).getValue()!=""){
				pmGrid.setRowColor(items[i], "#CCCCCC");
			}
		}	
	}
	function setSeledFile(){
		if(typeof parent.currDocField=="undefined"||parent.currDocField==""){
			//hintMsg("未指定回填文件域");
			return false;
		}
		var lockUserName = pmGrid.cells(pmGrid.getSelectedId(),6).getValue();
		if(typeof parent.currDocLock!="undefined"&&lockUserName!=""&&parent.currDocLock!=""){
			hintMsg("所选文档己被 " +lockUserName+"锁定");
			return false;
		}
		//try{
			var callback = parent.currDocCallback;
			var hiddenId =  parent.currDocField.id +"_hidden";
			parent.$(hiddenId).value=pmGrid.getSelectedId();
			parent.currDocField.value=pmGrid.cells(pmGrid.getSelectedId(),2).getValue();
			parent.pmDocSelW_ch.setModal(false);
			parent.pmDocSelW_ch.hide();
			if(typeof callback != "undefined" || callback != "")
				eval("parent." + callback + "()");
			parent.currDocCallback = "";
			parent.currDocField = "";
			parent.currDocLock = "";
			return true;
		//}catch(err){
		//	hintMsg("指定的回填文件域不存在");
			return false;
		//}
	}