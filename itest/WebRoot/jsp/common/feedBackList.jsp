<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>反馈管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY  style="overflow-x:hidden;overflow-y:hidden;">
	<script type="text/javascript">
	</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="fdCreateForm"name="fdCreateForm" namespace="" action="">
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="720">
					<tr class="ev_mypm"> 
						<td colspan="6" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue">建议在明细中最末行填上您联系方式邮箱,QQ或MS,您的建议被采纳后,新版发布时,我们第一时间联系您升级</div>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							反馈人明细:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<input id="customName" name="dto.feedBack.customName" Class="text_c" value="如XXX公司测试工程师张三"
								style="width:640;padding:2 0 0 4;" maxlength="100" onMouseOver="if(this.value=='如XXX公司测试工程师张三'){select();}" onblur="javascript:if(this.value==''){this.value='如XXX公司测试工程师张三';}" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							概述:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="fdDesc" name="dto.feedBack.fdDesc" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80"></ww:textfield>
						</td>
					</tr>
					<tr  class="odd_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0" nowrap>
							明细:
						</td>
						<td class="tdtxt" colspan="5"style="background-color: #f7f7f7;  width: 640;border-right:0">
							<ww:textarea id="fdReProStep"  name="dto.feedBack.fdReProStep" cssClass="text_c" cssStyle="width:640;padding:2 0 0 4;" rows="8"></ww:textarea>
						</td>
					</tr>
					</ww:form>
					<tr  class="ev_mypm">
					<td class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
					  <a class="bluebtn" href="javascript:void(0);"onclick="parent.fdW_ch.setModal(false);parent.fdW_ch.hide();"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="sendFd();"><span>确定</span> </a>
					<td>
					</tr>
			</table>
		</div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<script type="text/javascript">
	function sendFd(){
		if(!fdSubCheck())
			return;
		var url = conextPath+"/commonAction!sendFdBack.action";
		var rest = dhtmlxAjax.postSync(url,"fdCreateForm").xmlDoc.responseText; 
		if(rest=="success")	{
			$("cUMTxt").innerHTML="发送成功";
			$("fdCreateForm").reset();
		}else{
			$("cUMTxt").innerHTML="发送失败,请重试";
		}
	}
	function fdSubCheck(){
		if(isWhitespace($("customName").value)){
			$("cUMTxt").innerHTML="反馈人明细不能为空";
			$("customName").focus();
			return false;
		}if($("customName").value=="如XXX公司测试工程师张三"){
			$("cUMTxt").innerHTML="请填写反馈人明细";
			$("customName").focus();
			return false;
		}else if(isWhitespace($("fdDesc").value)){
			$("cUMTxt").innerHTML="概述不能为空";
			$("fdDesc").focus();
			return false;
		}else if(isWhitespace($("fdReProStep").value)){
			$("cUMTxt").innerHTML="明细不能为空";
			$("fdReProStep").focus();
			return false;
		}
		return true;
	}
	</script>
	</BODY>

</HTML>
