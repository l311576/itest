	if(myHome==""||myHome=="null"||myHome.length<4){   
		myHome = conextPath+"/bugManager/bugManagerAction!loadAllMyBug.action";   
	}else{   
		myHome = conextPath+myHome;   
	}   
	document.getElementById("logoTable").width = clientWidth - 10;   
	document.getElementById("menuObj").style.width = clientWidth - 10;   
	var scHeight = screen.height;   
	var myHeight = 0;   
	if(screen.width<=1024){   
		if(!_isIE)    
			myHeight = 580;   
		else   
		    myHeight = 600;   
	}else{   
		if(!_isIE)   
			myHeight = 590;   
		else   
		 myHeight = 610;   
	}   
	if(scHeight>768){   
		myHeight = myHeight + (scHeight-768);   
	}   
	if(_isIE)   
		myHeight = myHeight-145;   
	else   
	  myHeight = myHeight-100;   
	if(screen.width<=1024){   
		document.getElementById("mypmMainDiv").innerHTML="<iframe id=\"mypmMain\" name=\"mypmMain\" src=\""+myHome+"\" frameborder=\"0\" scrolling=\"no\"></iframe>";   
		document.getElementById("mypmMain").width = clientWidth;   
		parent.document.getElementById("mypmMain").height=myHeight;   
	}else{   
		document.getElementById("mypmMainDiv").innerHTML="<iframe id=\"mypmMain\" name=\"mypmMain\" src=\""+myHome+"\" frameborder=\"0\" scrolling=\"auto\"></iframe>";   
		document.getElementById("mypmMain").width = clientWidth;   
		parent.document.getElementById("mypmMain").height=myHeight;   
	}	   
	Date.prototype.format = function(format){   
	    var o ={   
	        "M+" : this.getMonth()+1,    
	        "d+" : this.getDate(),     
	        "h+" : this.getHours(),     
	        "m+" : this.getMinutes(),    
	        "s+" : this.getSeconds(),   
	        "q+" : Math.floor((this.getMonth()+3)/3),     
	        "S" : this.getMilliseconds()   
	    }   
	    if(/(y+)/.test(format)){   
	    	format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	    }   
	    for(var k in o){   
	    	if(new RegExp("("+ k +")").test(format)){   
	    		format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));   
	    	}   
	    }   
	    return format;   
	};   
	function setCookieVal(obj){   
		var date=new Date();   
		date.setTime(date.getTime()+ 2592000000);   
		document.cookie = obj + "; expires=" + date.toGMTString();   
	}   
	   
	function getCookieVal(key){   
		var strCookie = document.cookie;   
		var arrCookie = strCookie.split("; ");   
		var value = "";   
		for(var i = 0; i < arrCookie.length; i++){   
			var arr = arrCookie[i].split("=");   
			if(key == arr[0]){   
				value = arr[1];		   
				break;   
			}   
		}   
		return unescape(value);   
	}   
	importJs(conextPath+"/jsp/common/commonMsg.js");   
	function mainPostSub(url){   
		result = dhtmlxAjax.postSync(url,"").xmlDoc.responseText;   
	}	   
	mainPostSub(conextPath + "/navigate?reload=1&screenWh="+screen.width);   
	   
	if(result!=""){   
		eval(result);   
		dhtmlxAjax.post(conextPath + "/navigate?cacheMenu=1&screenWh="+screen.width,"");	   
	}else{   
		mainPostSub(conextPath + "/navigate?reload=1&screenWh="+screen.width);   
		eval(result);   
	}   
	function reLoadMyMenu(){   
		mainPostSub(conextPath + "/navigate?reload=1&screenWh="+screen.width);   
		eval(result);   
		if(currUrl == ""){   
			goMyHome('MypmTestPermission');   
			return;   
		}   
		if(currUrl.indexOf("?")>0){   
			currUrl = currUrl +"&MypmTestPermission=1&screenWh="+screen.width;   
		}else{   
			currUrl = currUrl +"?MypmTestPermission=1&screenWh="+screen.width;   
		}   
		parent.mypmMain.location = currUrl;   
		currUrl = "";	   
	}   
	var showMsg = true;   
	markCurrCustHome();   
	function markCurrCustHome(){   
		if(myHomeInit.length<=3){   
			if(myHomeInit.indexOf("1")>=0){   
				menu.setCheckboxState("selMsg", true);   
			}   
			if(myHomeInit.indexOf("2")>=0){   
				menu.setCheckboxState("selBug", true);   
			}   
		}else{   
			if(myHomeInit.indexOf("loadMyBug")>0){   
				if(menu.getItemText("sprHome")!="")   
					menu.setRadioChecked("ghome", "sprHome");   
			}else if(myHomeInit.indexOf("caseManagerAction!loadCase.action?dto.taskId=")>0){   
				if(menu.getItemText("caseHome")!="")   
					menu.setRadioChecked("ghome", "caseHome");   
			}else if(myHomeInit.indexOf("caseManagerAction!loadCase.action")>0){   
				if(menu.getItemText("testCaseMgr")!="")   
					menu.setRadioChecked("ghome", "testCaseMgr");   
			}else if(myHomeInit.indexOf("lastExeCase")>0){   
				if(menu.getItemText("lastExeCaseHome")!="")   
					menu.setRadioChecked("ghome", "lastExeCaseHome");   
			}else if(myHomeInit.indexOf("loadAllMyBug")>0){   
				if(menu.getItemText("allMySprHome")!="")   
					menu.setRadioChecked("ghome", "allMySprHome");   
			}else if(myHomeInit.indexOf("analysisAction!goAnalysisMain")>0){   
				if(menu.getItemText("repCenter")!="")   
					menu.setRadioChecked("ghome", "repCenter");   
			}else if(myHomeInit.indexOf("/outLineAction!index")>0){   
				if(menu.getItemText("testReqMgr")!="")   
					menu.setRadioChecked("ghome", "testReqMgr");   
			}   
		}   
	}   
	function goMyHome(appPara){   
		mainPostSub(conextPath + "/commonAction!getMyHome.action");   
		if(result.indexOf("success")>=0){   
			var homeUrl = result.split("^")[1];   
			if(homeUrl.length>3){   
				homeUrl =conextPath+ homeUrl;   
				if(typeof appPara !="undefined"){   
					if(homeUrl.indexOf("?")>0)   
						homeUrl = homeUrl +"&MypmTestPermission=1";   
					else   
					   homeUrl = homeUrl +"?MypmTestPermission=1";   
				}   
				parent.mypmMain.location=homeUrl;   
				return;   
			}else   
				homeUrl = conextPath+"/jsp/myhome/myHome.jsp";   
			    window.parent.location = homeUrl;   
		}else if(result!="overdue"){   
			hintMsg("加载我的首页发生错误");   
		}   
	}   
	function exeRelogin(){   
		window.parent.location.href= conextPath + "/navigate?reLogin=1";   
	}   
	function reLogin(){   
		cfDialog("exeRelogin","您确定要退出?",false);	   
	}   
	function menuCheckboxClick(id, state) {   
		var custHome = ""   
		if(menu.getCheckboxState('selMsg')&&id!="selMsg"){   
			custHome ="1";   
		}   
		if(menu.getCheckboxState('selBug')&&id!="selBug"){   
			custHome +="2";   
		}   
		var currStatus = state?"unchecked":"checked";   
		if(currStatus=="checked"){   
			if(id=="selMsg"){   
				custHome ="1";   
			}   
			if(id=="selBug"){   
				custHome +="2";   
			}		   
		}   
		var url = conextPath+"/commonAction!customMyHome.action?dto.myHomeUrl="+custHome;   
		dhtmlxAjax.postSync(url,"").xmlDoc.responseText;    
		menu.setRadioChecked("ghome", "cancelLstHome");   
		return true;   
	}   
   
	function menuRadioClick(group, idChecked, idClicked) {   
		var custHome="";   
		if(idClicked=="cancelLstHome"){   
			return true;   
		}   
		if(idClicked=="sprHome"){   
			custHome="/bugManager/bugManagerAction!loadMyBug.action?dto.taskId=";   
			try{   
				custHome = custHome+parent.mypmMain.$("customSprHomeTaskId").value;   
			}catch(e){   
				hintMsg("当前不在项目(任务)BUG列表中，不能定制");   
				return false;   
			}		   
		}else if(idClicked=="caseHome"){   
			custHome="/caseManager/caseManagerAction!loadCase.action?dto.taskId=";   
			try{   
				custHome = custHome+parent.mypmMain.$("customCaseHomeTaskId").value;   
			}catch(e){   
				hintMsg("当前不在用例列表中，不能定制");   
				return false;   
			}		   
		}else if(idClicked=="lastExeCaseHome"){   
			custHome="/caseManager/caseManagerAction!lastExeCase.action";		   
		}else if(idClicked=="allMySprHome"){   
			custHome="/bugManager/bugManagerAction!loadAllMyBug.action?dto.allTestTask=true";		   
		}else if(idClicked=="repCenter"){   
			custHome="/analysis/analysisAction!goAnalysisMain.action";		   
		} else if(idClicked=="testReqMgr"){   
			custHome="/outLineManager/outLineAction!index.action";		   
		}else if(idClicked=="testCaseMgr"){   
			custHome="/caseManager/caseManagerAction!loadCase.action";		   
		}   
		var url = conextPath+"/commonAction!customMyHome.action?dto.myHomeUrl="+custHome;   
		dhtmlxAjax.postSync(url,"").xmlDoc.responseText;    
		setCheckboxUnchk();   
		return true;   
	}   
	function setCheckboxUnchk(lstHomeId){   
		menu.setCheckboxState("selMsg", false);   
		menu.setCheckboxState("selBug", false);   
		if(typeof(lstHomeId) !="undefined")	   
			menu.setRadioChecked("ghome", lstHomeId);   
	}   
	var tutoriaW_ch,proW_ch,licenseW_ch;    
	function viewGuid(){   
		tutoriaW_ch = initW_ch(tutoriaW_ch, "", false, 650, 450,'tutoriaW_ch');   
		tutoriaW_ch.attachURL(conextPath+"/tutoria.html");   
		tutoriaW_ch.setText("MYPM简易向导");	   
	    tutoriaW_ch.show();   
	    tutoriaW_ch.bringToTop();   
	    tutoriaW_ch.setModal(false);   
	}   
	function showProDetl(operCmd){    
		if(operCmd=="license"){   
			licenseW_ch = initW_ch(licenseW_ch, "", false, 760, 245,'licenseW_ch');    
			//licenseW_ch.button("close").attachEvent("onClick", function(){   
			//	proW_ch.show();proW_ch.bringToTop();   
			//});   
			licenseW_ch.attachURL(conextPath+"/jsp/licenseInfo.jsp");    
			licenseW_ch.setText("授权信息");	    
		    licenseW_ch.show();    
		    licenseW_ch.bringToTop();    
		    licenseW_ch.setModal(false);    
		    try{proW_ch.hide();}catch(err)	{}   
		}else if(operCmd!="helpPro"){    
			proW_ch = initW_ch(proW_ch, "", false, 700, 350,'proW_ch');    
			proW_ch.attachURL(conextPath+"/jsp/aboutMypm.jsp");    
			proW_ch.setText("关于MYPM");	    
		    proW_ch.show();    
		    proW_ch.bringToTop();    
		    proW_ch.setModal(false);    
		}else{    
			viewProDoc();    
		}    
	}    
	var myInfoW_ch;    
	function upMyInfo(){    
		if(_isIE){    
			myInfoW_ch = initW_ch(myInfoW_ch, "", true, 470, 316,'myInfoW_ch');    
		}else{    
			myInfoW_ch = initW_ch(myInfoW_ch, "", true, 470, 335,'myInfoW_ch');    
		}    
		myInfoW_ch.attachURL(conextPath+"/userManager/userManagerAction!setMyInfoInit.action");    
		myInfoW_ch.setText("个人信息维护");	    
	    myInfoW_ch.show();    
	    myInfoW_ch.bringToTop();    
	    myInfoW_ch.setModal(true);	    
	    return;    
	}    
	var fdW_ch,ulAppW_ch,ulActiveW_ch;     
	function sendFdInit(){     
		fdW_ch = initW_ch(fdW_ch, "", true, 760, 270,'fdW_ch');     
		fdW_ch.attachURL(conextPath+"/jsp/common/feedBackList.jsp");     
		fdW_ch.setText("向MYPM反馈|建议");	     
	    fdW_ch.show();     
	    fdW_ch.bringToTop();     
	    fdW_ch.setModal(true);	     
	    return;	     
	}     
	function ulAppInit(operCmd){     
		if(navigator.userAgent.indexOf("Chrome")>0){ 
			ulAppW_ch = initW_ch(ulAppW_ch, "", true, 760, 335,'ulAppW_ch');    
		}else if(_isIE){  
			ulAppW_ch = initW_ch(ulAppW_ch, "", true, 760, 325,'ulAppW_ch');    
		}else{  
			ulAppW_ch = initW_ch(ulAppW_ch, "", true, 760, 305,'ulAppW_ch');  
		}  
		  
		ulAppW_ch.button("close").attachEvent("onClick", function(){    
			licenseW_ch.hide();    
		});    
		var url = conextPath+"/commonAction!userLicenseMgr.action?dto.operCmd="+operCmd;       
		ulAppW_ch.attachURL(url);     
		ulAppW_ch.setText("申请扩容激活码");	     
	    ulAppW_ch.show();     
	    ulAppW_ch.bringToTop();     
	    ulAppW_ch.setModal(true);	     
	    ulAppW_ch.denyMove();    
	    return;	     
	}     
	function rlAppInit(operCmd){     
		if(navigator.userAgent.indexOf("Chrome")>0){ 
			ulAppW_ch = initW_ch(ulAppW_ch, "", true, 760, 335,'ulAppW_ch');    
		}else if(_isIE){  
			ulAppW_ch = initW_ch(ulAppW_ch, "", true, 760, 325,'ulAppW_ch');    
		}else{  
			ulAppW_ch = initW_ch(ulAppW_ch, "", true, 760, 305,'ulAppW_ch');  
		}    
		ulAppW_ch.button("close").attachEvent("onClick", function(){    
			licenseW_ch.hide();    
		});    
		var url = conextPath+"/commonAction!reptLicenseMgr.action?dto.operCmd="+operCmd;       
		ulAppW_ch.attachURL(url);     
		ulAppW_ch.setText("申请度量激活码");	     
	    ulAppW_ch.show();     
	    ulAppW_ch.bringToTop();     
	    ulAppW_ch.setModal(true);	     
	    ulAppW_ch.denyMove();    
	    return;	     
	}     
	function setActiveCodeInit(){      
		ulActiveW_ch = initW_ch(ulActiveW_ch, "", true, 760, 270,'ulActiveW_ch');     
		var url = conextPath+"/jsp/common/setActiveCode.jsp";       
		ulActiveW_ch.attachURL(url);      
		ulActiveW_ch.setText("MYPM激活");	      
	    ulActiveW_ch.show();      
	    ulActiveW_ch.bringToTop();      
	    ulActiveW_ch.setModal(true);	      
	    ulActiveW_ch.denyMove();     
	    return;	      
	}      
