	var taskCustomArray,statusArray;
	var whW_ch, whGrid;
	
	function doOnCellEdit_WH(stage,rowId,cellInd){
		if(typeof(cuW_ch) != "undefined" && !cuW_ch.isHidden()){
			//var editorText = editor.getContent();
			//if(editorText != "")
				//editorText += "<br>";
			var smData = whGrid.cells(rowId,1).getValue() + "  ";
			smData += whGrid.cells(rowId,2).getValue() + "  ";
			smData += whGrid.cells(rowId,3).getValue() + "  ";
			smData += whGrid.cells(rowId,4).getValue();
			editor.insertContent(smData);
			//editor.setContent(editorText + smData);
		}
		return false;
	}
	
	function searchCurrWh(operationId, pcDate, obj){
		if(pcDate == ""){
			showMessage(false, "查看当日所填工时信息", "请选择进度时间");
			return;
		}
		var result = dhtmlxAjax.postSync(conextPath + "/task/taskAction!searchCurrWh.action?taskDto.progressLog.pcDate=" + pcDate + "&taskDto.operationId=" + operationId, "").xmlDoc.responseText;
		if(result != "failed" && result != ""){
			whW_ch = initWch(whW_ch,  "whWinDiv", false, 400, 150, "", "查看当日所填工时信息");
			whW_ch.bringToTop();
			if(result == "noData"){
				showMessage(false, "查看当日所填工时信息", "您还没有填写工时数据");
				whW_ch.hide();
				return;
			}
			if(typeof(whGrid) == "undefined"){
			    taskCustomArray = new Array("通用任务", "项目任务");
			    statusArray = new Array("未审批", "已审批", "删除");
				whGrid = new dhtmlXGridObject('whDiv');
				whGrid.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
				whGrid.setHeader("&nbsp;,&nbsp;,任务(项目)名称,任务类别,所用工时(h),填写人,&nbsp;状态");
				whGrid.setInitWidths("0,40,*,80,90,160,70");
				whGrid.setColAlign("center,center,left,center,center,left,center");
				whGrid.setColSorting("int,int,str,str,str,str,str");
				whGrid.setColTypes("ro,ed,ed,ed,ed,ed,ed");
				whGrid.attachEvent("onEditCell",doOnCellEdit_WH);
				whGrid.enableAutoHeight(true, 150);
				whGrid.init();
				whGrid.enableRowsHover(true, "red");
				whGrid.enableTooltips("false,false,true,true,true,true,true");
				whGrid.setSkin("light");
			}
			whGrid.clearAll();
			var whs = eval("(" + result +")");
			whGrid.parse(whs, "json");
			setWHRowNums();
			setWinSize(obj);
		}else{
			showMessage(false, "查看当所填日工时信息", "加载数据错误");
		}
	}
	
	function setWHRowNums(){
		var allItems = whGrid.getAllItemIds();
		if(allItems == "")return;
		var items = allItems.split(',');
		var type, status;
		for(var i = 0; i < items.length; i++){
			whGrid.cells(items[i],1).setValue(i + 1);
			type = whGrid.cells(items[i],3).getValue();
			whGrid.cells(items[i],3).setValue(taskCustomArray[type]);
			status = whGrid.cells(items[i],6).getValue();
			whGrid.cells(items[i],6).setValue(statusArray[status]);
		}
	}
	
	function setWinSize(obj){
		whW_ch.setPosition(50,50);
		var oRect = obj.getBoundingClientRect();
		whW_ch.setPosition(oRect.right + 30,oRect.top);
		whW_ch.setDimension(560, $3("whWinDiv").offsetHeight + 20);
	}