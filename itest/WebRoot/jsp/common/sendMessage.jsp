<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/sendMessage.js");
	</script>
	<div id="addMsgDiv" class="gridbox_light" style="border:0px;display:none;">
		<div style="width:100%" class="objbox">
	     	<ww:form theme="simple" method="post" id="addMsgF" name="addMsgF" namespace="/report" action="">
	     		<input type="hidden" id="msgCreateId" name="reportDto.report.createId" />
	     		<input type="hidden" id="subjectMsg" name="reportDto.report.subject" />
		      	<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
		      		<tr class="ev_mypm">
		      			<td class="leftM_center" style="border-right:0;color:red;">消息内容:</td>
		      			<td>
		      				<ww:textarea id="msgRemark" name="reportDto.report.remark" cssStyle="width:292;width:286px !ie;height:50px !ie;" cssClass="textarea" rows="2" ></ww:textarea>
		      			</td>
		      		</tr>
		      		<tr class="odd_mypm">
		      			<td colspan="2"><div id="msgText" class="waring">&nbsp;</div></td>
		      		</tr>
		      		<tr class="ev_mypm">
		      			<td colspan="2" align="right"><img src="<%=request.getContextPath()%>/jsp/common/images/sendMsg.gif" onclick="sendMsg();" title="发送消息" style="cursor:pointer;" />&nbsp;&nbsp;<img src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formReset('addMsgF', 'msgRemark');$('msgText').innerHTML = '';" title="重置" style="cursor:pointer;" />&nbsp;</td>
		      		</tr>
		      		<tr class="odd_mypm">
		      			<td colspan="2">&nbsp;</td>
		      		</tr>
		      	</table>
	      	</ww:form>
	     </div>
  	</div>
  	<div id="msgDetalDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		<div class="objbox" style="overflow:auto;width:100%;">
			 <input type="hidden" id="initContent" name="initContent"/>
			 <input type="hidden" id="startDateInit" name="startDateInit"/>
			 <input type="hidden" id="sendName" name="sendName"/>
			 <input type="hidden"  id="attachUrl" name="dto.broMsg.attachUrl"/>
				<ww:form theme="simple" method="post" id="createForm"name="createForm" namespace="" action="">
				<input type="hidden"  id="logicId" name="dto.broMsg.logicId"/>
				<input type="hidden" id="state" name="dto.broMsg.state"/>
				<input type="hidden"  id="senderId" name="dto.broMsg.senderId"/>
				<input type="hidden"  id="sendDate" name="dto.broMsg.sendDate"/>
				<input type="hidden"  id="recpiUserId" name="dto.broMsg.recpiUserId"/>
				<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" id="msgDetalTab">
				<tr><td width="600"colspan="4" style="border-right:0">&nbsp;</td></tr>
				<tr><td class="rightM_center" width="80" align="right">标题:</td>
					<td class="dataM_left" width="560" colspan="3"align="left">
						<ww:textfield id="msgTitle" name="dto.broMsg.title" readonly="true" cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
				    </td>
				</tr>	
				<tr>
					<td class="rightM_center" align="right"width="80" nowrap style="border-right:0">生效日期:</td>
				    <td class="dataM_left" width="100" align="left" style="border-right:0">
						<ww:textfield id="startDate" readonly="true" name="dto.broMsg.startDate" cssStyle="padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
				    </td>
				  	<td class="rightM_center" width="80" align="right" style="border-right:0">失效日期:</td>
				    <td class="dataM_left" width="100" align="left" style="border-right:0">
						<ww:textfield id="overdueDate" readonly="true" name="dto.broMsg.overdueDate" cssStyle="padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
				    </td>
				  </tr>	
				  <tr>
				  	 <td class="rightM_center" width="80" align="right">类型:</td>
				    <td class="dataM_left" width="100" align="left">
						<ww:select id="msgType" name="dto.broMsg.msgType"
							list="#{-1:'',0:'广播',1:'限定接收人'}" headerValue="-1" cssStyle="width:120;" cssClass="select_c">
						</ww:select>
				    </td>
				  	 <td class="rightM_center" width="80" align="right">同时Mail告知:</td>
				    <td  width="100" align="left" class="dataM_left">
				        <ww:checkbox name="dto.broMsg.mailFlg" id="mailFlg" disabled="true" value="FLASE" fieldValue="1" cssClass="text_c"/>
				    </td>
				  </tr>	
				  <tr id="recpiUserIdTr" style="display:none">
				  	<td class="rightM_center" width="80" align="right">接收人:</td>
				    <td class="dataM_left" width="560" colspan="3"align="left">
						<ww:textfield id="recpiUserName" name="recpiUserName"readonly="true"   cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
				    </td>
				  </tr>	
				  <tr>
				  	<td class="rightM_center" width="80" align="right">内容:</td>
				    <td class="dataM_left" width="560" colspan="3"align="left">
						<div id="msgContentDiv" style="width:560;height:220px;"></div>
				    </td>
				  </tr>
				<tr>	
			</table>
		</ww:form>
		</div>
</div>	