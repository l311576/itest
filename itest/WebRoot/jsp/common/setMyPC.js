	var pc_ch, cusW_ch, parentWHW_ch, setMyPC100W_ch, pcHistoryW_ch, pcHistoryGrid, pcArray;
	function initPCWindow(taskId, projectId, taskName){
		pc_ch = initWch(pc_ch,  "setMyPCDiv", false, 380, 335, "", "填写进度");
		//$("setMyPCSaveImg").style.display = "";$("setMyPCResetImg").style.display = "";
		var dateStr = formatDateYMD(new Date());
		pc_ch.button("close").attachEvent("onClick", function(){
			pc_ch.hide();
			if(typeof(mypmCalendar_ch) != "undefined")mypmCalendar_ch.hide();
			if(typeof whW_ch != "undefined")whW_ch.hide();
			if(typeof cusW_ch != "undefined")cusW_ch.hide();
			if(typeof parentWHW_ch != "undefined")parentWHW_ch.hide();
			if(typeof pcHistoryW_ch != "undefined")pcHistoryW_ch.hide();});
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!setMYPCInit.action?taskDto.task.id=" + taskId + "&taskDto.task.projectId=" + projectId + "&taskDto.progressLog.pcDate=" + dateStr, "");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
	  	$3("percentCompleted").disabled = false;
		clearMyPCMessage();
		if(result != "failed" && result != ""){
			if(result == "noResource"){
				pc_ch.hide();
				showMessage(false, "设置进度", "此任务不包含工时资源 不能添加进度");	
				return;
			}
			if(result == "hasSon"){
				//showMessage(false, "设置进度", "只有子任务才能填写进度");
				//return;
				pc_ch.hide();
				addParentWH(taskName, taskId);
				return;
			}
			var resultTemp = result.split("$");
			var results = resultTemp[0].split("^");
			controlLength("setMyPC_name", results[0], 40);
			$3("setMyPC_TS").innerHTML = results[1] + "&nbsp;";
			$3("setMyPC_PS").innerHTML = results[2] + "&nbsp;";
			$3("setMyPC_PE").innerHTML = results[3] + "&nbsp;";
			$3("setMyPC_FS").innerHTML = results[4] + "&nbsp;";
			$3("setMyPC_FE").innerHTML = results[5] + "&nbsp;";
			$3("setMyPC_PD").innerHTML = results[6] + "&nbsp;";
			$3("setMyPC_FD").innerHTML = results[7] + "&nbsp;";
			$3("setMyPC_TP").innerHTML = results[8] + "&nbsp;";
			$3("perviousPCTime").innerHTML = results[9] + "&nbsp;";
			$3("perviousPC").innerHTML = results[10] + "&nbsp;";
			$3("perviousPCResult").innerHTML = results[11] + "&nbsp;";
			//if(results[12] == 0 || results[12] == -1)
				$3("percentCompleted").value = results[10];
			//else
				//$3("percentCompleted").value = pmGrid.cells(taskId,8).getValue();
			$3("pcDate").value = dateStr;
			$3("progressId_T").value = results[13];
			$3("pcTaskId").value = results[14];
			$3("remark_PC").innerHTML = "";
			$3("setMyPCResetImg").style.display = "";$3("setMyPCSaveImg").style.display = "";
			if(resultTemp[1] == "moreResource"){
				$3("addTaskWHImg").style.display = "";
			}else{
				$3("addTaskWHImg").style.display = "none";
			}
			if(resultTemp[2] == "no"){
			}else{
				$3("wh").value = resultTemp[2];
			}
			if(resultTemp[3] == "1"){
				$3("wh").disabled = true;
			}else{
				$3("wh").disabled = false;
			}
			if(results[9] == $3("pcDate").value){
				var msg = $("perviousPCResult").innerHTML;
				if( msg == "未审批&nbsp;"){
					$3("setMyPCText").innerHTML = "进度已经设置 但尚未审批 若确认修改 请选择'修改'";$3("percentCompleted").disabled = false;
					$3("setMyPCResetImg").style.display = "";$3("setMyPCSaveImg").style.display = "";
				}else if(msg == "审批通过&nbsp;"){
					$3("setMyPCText").innerHTML = "进度已经审批 不能修改";$3("percentCompleted").disabled = true;
					$3("setMyPCResetImg").style.display = "none";$3("setMyPCSaveImg").style.display = "none";
				}else{
					$3("setMyPCResetImg").style.display = "";$3("setMyPCSaveImg").style.display = "";
				}
			}else{
				$3("setMyPCResetImg").style.display = "";$3("setMyPCSaveImg").style.display = "";
			}
			if(resultTemp[4] == "noPcAssume"){
				addParentWH($3('setMyPC_name').innerHTML, $3('pcTaskId').value, $3('pcDate').value);pc_ch.hide();
			}
		}else{
			showMessage(false, "填写进度", "进度信息初始化失败");
		}
		initDhtmlxCalendar();
	}
	
	function clearMyPCMessage(){
		$3("perviousPCTime").innerHTML = "";
		$3("perviousPC").innerHTML = "";
		$3("perviousPCResult").innerHTML = "";
		$3("remark_PC").innerHTML = ""; 
		$3("setMyPC_name").innerHTML = "";
		$3("setMyPC_TS").innerHTML = "";
		$3("setMyPC_PS").innerHTML = "";
		$3("setMyPC_FS").innerHTML = "";
		$3("setMyPC_PE").innerHTML = "";
		$3("setMyPC_FE").innerHTML = "";
		$3("setMyPC_PD").innerHTML = "";
		$3("setMyPC_FD").innerHTML = "";
		$3("setMyPC_TP").innerHTML = "";
		$3("setMyPCText").innerHTML = "";
		$3("isCreatePC0").disabled = false;
		formReset("setMyPCF", "percentCompleted");
	}
	
	function setMYPC(){
		if($3("pcDate").value == ""){
			$3("setMyPCText").innerHTML = "请填写进度时间";
			$3("pcDate").focus();
			return;
		}
		if(!isDigit($3("percentCompleted").value, false)){
			$3("setMyPCText").innerHTML = "请填写数字";
			$3("percentCompleted").focus();
			return;
		}
		if(parseInt($3("percentCompleted").value) < 0 || parseInt($3("percentCompleted").value) > 100){
			$3("setMyPCText").innerHTML = "进度的范围在0至100之间";
			$3("percentCompleted").focus();
			return;
		}
		if($3("wh").value == ""){
			$3("setMyPCText").innerHTML = "工时不能为空 请填写数字";
			$3("wh").focus();
			return;
		}
		if(!isFloat($3("wh").value, false)){
			$3("setMyPCText").innerHTML = "请填写数字";
			$3("wh").focus();
			return;
		}
		if(parseInt($3("wh").value) < 0 || parseInt($3("wh").value) > 48){
			$3("setMyPCText").innerHTML = "工时数字范围在0至48之间";
			$3("wh").focus();
			return;
		}
		var dateStr = formatDateYMD(new Date());
		if($("pcDate").value > dateStr){
			$3("setMyPCText").innerHTML = "只能填写今天或之前的进度  请选择正确的时间";
			return;
		}
		var p_pc = trim(replaceAll($3("perviousPC").innerHTML, "&nbsp;", "")); var p_pr = trim(replaceAll($3("perviousPCResult").innerHTML, "&nbsp;", ""));
		var p_pd = $3("pcDate").value; var p_pt = trim(replaceAll($3("perviousPCTime").innerHTML, "&nbsp;", ""));
		if(p_pc == "100" && p_pr == "审批通过" && p_pd >= p_pt){
			setMyPC100W_ch = initWch(setMyPC100W_ch,  "setMyPC100MessageDiv", true, 300, 123, "", "填写进度");
		}else{
			excuteSetPC();
		}
	}
	
	function excuteSetPC(){
		if($3("isCreatePC0").checked){
			$3("progressId").value = $3("progressId_T").value;
		}else{
			$3("progressId").value = "";
		}
		if(typeof setMyPC100W_ch != "undefined"){setMyPC100W_ch.hide();setMyPC100W_ch.setModal(false);}
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!setMYPC.action", "setMyPCF");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			var results = result.split("$");
			if(results[1] != "")
				$3("progressId_T").value = results[1];
			if(results[0] == "success"){
				$3("setMyPCText").innerHTML = "进度设置成功 但尚未审批";
			}else if(results[0] == "onlyOne"){
				$3("setMyPCText").innerHTML = $3("pcDate").value + " 进度已经设置 但尚未审批 若确认修改 请选择'修改'";
				return;
			}else if(results[0] == "alreadyApproval"){
				$3("setMyPCText").innerHTML = $3("pcDate").value + " 进度已经审批 不能再次设置";
			}else if(results[0] == "noExist"){
				$3("setMyPCText").innerHTML = $3("pcDate").value + " 进度没有设置 若确认设置 请选择'添加新进度'";
				return;
			}else if(results[0] == "afterNow"){
				$3("setMyPCText").innerHTML = "只能填写今天或之前的进度  请选择正确的时间";
				return;
			}else if(results[0] == "noPerminsions"){
				$3("setMyPCText").innerHTML = "进度设置只有任务参与人能填写 您没有权限修改此任务的进度";
			}else if(results[0] == "lockProjectForPC"){
				$3("setMyPCText").innerHTML = "项目未完成计划 不能修改进度";
			}else if(results[0] == "endTask"){
				$3("setMyPCText").innerHTML = "此任务已被人为终止 不能修改进度";
			}else{
				$3("setMyPCText").innerHTML = "进度设置失败";
			}
			$3("setMyPCResetImg").style.display = "none";$3("setMyPCSaveImg").style.display = "none";$3("percentCompleted").disabled = true;
		}else{
			$3("setMyPCText").innerHTML = "进度设置出现错误";
		}
	}
	
	function addCustom(date, obj, taskId){
		cusW_ch = initWch(cusW_ch,  "addCustomTaskDiv", false, 370, 215, "", "填写通用任务工时信息");
		cusW_ch.bringToTop();
		var oRect = obj.getBoundingClientRect();
		cusW_ch.setPosition(oRect.right + 105,oRect.top);
		formReset('addCustomTaskLogF', 'custom_wh');
		$3("progressDateString").value = date;
		if(typeof(taskId) == "undefined")taskId = "";
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!addCustomTaskLogInit.action?taskDto.taskUserLog.progressDateString=" + date + "&taskDto.taskUserLog.taskId=" + taskId, "");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			var results = result.split("$");
			selectInit("customTaskId", results[1], null, "custom_cTM", "没有可用的通用任务");
			selectInit("customProjectId", results[2], "");
			if(results[3] == "0.0"){
				$3("custom_cTM").innerHTML = "提示:您还没有填写工时";
			}else{
				$3("custom_cTM").innerHTML = "提示:您已经填写了工时" + results[3] + "(h)";
			}
			$3("customProjectId").value = results[4];
		}else{
			showMessage(false, "填写通用任务工时信息", "不能加载数据");
			cusW_ch.hide();
			return;
		}
	}
	
	function getParentWHInit(date, type){
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!addTaskUserLogInit.action?taskDto.taskUserLog.progressDateString=" + date, "");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			if(type == 0){
				if(result == "0.0"){
					$3("custom_cTMParent").innerHTML = "提示:您还没有填写工时";
				}else{
					$3("custom_cTMParent").innerHTML = "提示:您已经填写了工时" + result + "(h)";
				}
			}else{
				if(result == "0.0"){
					$3("custom_cTM").innerHTML = "提示:您还没有填写工时";
				}else{
					$3("custom_cTM").innerHTML = "提示:您已经填写了工时" + result + "(h)";
				}
			}
		}
	}
	
	function addParentWH(taskName, taskId, date){
		parentWHW_ch = initWch(parentWHW_ch,  "setParentTaskWHDiv", false, 290, 145, "", "填写任务工时信息");
		var oRect = $("addTaskWHImg").getBoundingClientRect();
		if(oRect.top > 0)parentWHW_ch.setPosition(oRect.right + 30,oRect.top);
		getParentWHInit(date, 0);
		parentWHW_ch.bringToTop();
		formReset('addParentTaskWHF', 'custom_whParent');
		$3("taskNameWHDiv").innerHTML = getHrefValueSimple(taskName);
		$3("taskUserLongTaskId").value = taskId;
		if(typeof date != "undefined")$3("progressDateStringParent").value = date;
		else $3("progressDateStringParent").value = formatDateYMD(new Date());
	}
	
	function searchTaskPCHistory(taskName, taskId, obj){
		var result = dhtmlxAjax.postSync(conextPath + "/task/taskAction!searchTaskPCHistory.action?taskDto.progressLog.taskId=" + taskId, "").xmlDoc.responseText;
		if(result != "failed" && result != ""){
			pcHistoryW_ch = initWch(pcHistoryW_ch,  "pcHistoryWinDiv", false, 530, 125, "", "任务进度历史信息");
			$("pcHistoryText").innerHTML = taskName + " 进度历史";
			pcHistoryW_ch.bringToTop();
			if(result == "noData"){
				showMessage(false, "任务进度历史信息", "此任务尚无进度数据");
				pcHistoryW_ch.hide();
				return;
			}
			if(typeof(pcHistoryGrid) == "undefined"){
			    pcArray = new Array();
				pcArray[-1] = "\u5ba1\u6279\u672a\u901a\u8fc7";
				pcArray[0] = "\u672a\u5ba1\u6279";
				pcArray[1] = "\u5ba1\u6279\u901a\u8fc7";
				pcArray[2] = "\u6807\u8bb0\u4e3a\u975e\u6cd5\u6570\u636e";
				pcHistoryGrid = new dhtmlXGridObject('pcHistoryDiv');
				pcHistoryGrid.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
				pcHistoryGrid.setHeader("&nbsp;,&nbsp;,进度时间,进度,所用工时(h),填写人,审批人,审批结果");
				pcHistoryGrid.setInitWidths("0,40,75,50,90,100,100,70");
				pcHistoryGrid.setColAlign("center,center,center,center,center,left,left,center");
				pcHistoryGrid.setColSorting("int,int,str,int,int,str,str,str");
				pcHistoryGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
				pcHistoryGrid.enableAutoHeight(true, 120);
				pcHistoryGrid.init();
				pcHistoryGrid.enableRowsHover(true, "red");
				pcHistoryGrid.enableTooltips("false,false,true,true,true,true,true,true");
				pcHistoryGrid.setSkin("light");
			}
			pcHistoryGrid.clearAll();
			var results = result.split("$");
			if(results[1] == "")return;
			var history = eval("(" + results[1] +")");
			pcHistoryGrid.parse(history, "json");
			setHistoryRowNums(results[0]);
			resetWinHeight(pcHistoryW_ch,  "pcHistoryDiv", 530, 145, obj.id, 25)
		}else{
			showMessage(false, "任务进度历史信息", "加载数据错误");
		}
	}
	
	function setHistoryRowNums(apUsers){
		var allItems = pcHistoryGrid.getAllItemIds();
		if(allItems == "")return;
		var items = allItems.split(',');
		var status, apId, cutStr; length;
		for(var i = 0; i < items.length; i++){
			pcHistoryGrid.cells(items[i],1).setValue(i + 1);
			status = pcHistoryGrid.cells(items[i],7).getValue();
			pcHistoryGrid.cells(items[i],7).setValue(pcArray[status]);
			if(apUsers != ""){
				apId = pcHistoryGrid.cells(items[i],6).getValue();
				length = apId.length + 1;
				if(apId != ""){
					cutStr = apUsers.substring(apUsers.indexOf(apId + "^") + length, apUsers.length);
					cutStr = cutStr.substring(0, cutStr.indexOf(","));
					pcHistoryGrid.cells(items[i],6).setValue(cutStr);
				}
			}
		}
	}
	
	function buttomDis(flag){
		if(flag){
			$3("addButtom").style.display = "";
		}else{
			$3("addButtom").style.display = "none";
		}
	}

	function checkCustomTaskLog(){
		if($3("custom_wh").value == ""){
			$3("custom_cTM").innerHTML = "工时不能为空 请填写数字";
			$3("custom_wh").focus();
			return false;
		}
		if($3("progressDateString").value == ""){
			$3("custom_cTM").innerHTML = "填写时间不能为空";
			return false;
		}
		if(!isFloat($3("custom_wh").value, false)){
			$3("custom_cTM").innerHTML = "请填写数字";
			$3("custom_wh").focus();
			return false;
		}
		if(parseInt($3("custom_wh").value) <= 0 || parseInt($3("custom_wh").value) > 48){
			$3("custom_cTM").innerHTML = "工时数字范围在0至48之间";
			$3("custom_wh").focus();
			return false;
		}
		return true;
	}
	
	function addCustomTaskLog(){
		createCustomTaskLog();
	}
	
	function createCustomTaskLog(){
		if(!checkCustomTaskLog()){
			return;
		}
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!addCustomTaskLog.action", "addCustomTaskLogF");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			if(result == "lockProjectForStatus"){
				$3("custom_cTM").innerHTML = "项目未被激活 不能设置工时";
			}else{
				$3('custom_wh').value = '';$3('custom_wh').focus();
				$3("custom_cTM").innerHTML = "添加通用任务工时成功";
			}
		}else{
			$3("custom_cTM").innerHTML = "添加通用任务工时失败";
		}
	}
	
	function addParentTaskWH(){
		createParentTaskWH();
	}
	
	function createParentTaskWH(){
		if(!checkParentTaskWH()){
			return;
		}
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!addParentTaskWh.action", "addParentTaskWHF");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			if(result == "lockProjectForStatus"){
				$3("custom_cTMParent").innerHTML = "项目未被激活 不能设置工时";
			}else{
				$3('custom_whParent').value = '';$3('custom_whParent').focus();
				$3("custom_cTMParent").innerHTML = "添加任务工时成功";
			}
		}else{
			$3("custom_cTMParent").innerHTML = "添加任务工时失败";
		}
	}
	
	function checkParentTaskWH(){
		if($3("custom_whParent").value == ""){
			$3("custom_cTMParent").innerHTML = "工时不能为空 请填写数字";
			$3("custom_whParent").focus();
			return false;
		}
		if($3("progressDateStringParent").value == ""){
			$3("custom_cTMParent").innerHTML = "填写时间不能为空";
			return false;
		}
		if(!isFloat($3("custom_whParent").value, false)){
			$3("custom_cTMParent").innerHTML = "请填写数字";
			$3("custom_whParent").focus();
			return false;
		}
		if(parseInt($3("custom_cTMParent").value) <= 0 || parseInt($3("custom_cTMParent").value) > 48){
			$3("custom_cTMParent").innerHTML = "工时数字范围在0至48之间";
			$3("custom_cTMParent").focus();
			return false;
		}
		return true;
	}
	
	function chosePCDate(){
		$3("setMyPCText").innerHTML = "";
		var pcTaskId = $("pcTaskId").value;
		var pcDate = $("pcDate").value;
		var dateStr = formatDateYMD(new Date());
		if(pcDate > dateStr){
			$3("setMyPCText").innerHTML = "只能填写今天或之前的进度  请选择正确的时间";
			$3("setMyPCResetImg").style.display = "none";$3("setMyPCSaveImg").style.display = "none";
			return;
		}
		if(typeof whW_ch != "undefined")whW_ch.hide();
		if(typeof cusW_ch != "undefined")cusW_ch.hide();
		if(typeof parentWHW_ch != "undefined")parentWHW_ch.hide();
		if(typeof pcHistoryW_ch != "undefined")pcHistoryW_ch.hide();
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!chosePCDate.action?taskDto.taskUserLog.taskId=" + pcTaskId + "&taskDto.taskUserLog.progressDateString=" + pcDate, "");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			var results = result.split("$");
			if(results[0] == "no"){
				$3("wh").value = "";
			}else{
				$3("wh").value = results[0];
			}
			if(results[2] == "1"){
				$3("wh").disabled = true;
			}else{
				$3("wh").disabled = false;
			}
			if(results[3] != "")$3("percentCompleted").value = results[3];else $3("percentCompleted").value = "";
			if(results[1] == "no"){
				$3("setMyPCText").innerHTML = "当日进度已经审批 不能修改";$3("percentCompleted").disabled = true;
				$3("setMyPCResetImg").style.display = "none";$3("setMyPCSaveImg").style.display = "none";
			}else if(results[1] == "update"){
				$3("setMyPCText").innerHTML = "进度已经设置 但尚未审批 若确认修改 请选择'修改'";$3("percentCompleted").disabled = false;
				$3("setMyPCResetImg").style.display = "";$3("setMyPCSaveImg").style.display = "";
			}else{
				$3("setMyPCText").innerHTML = "";$3("percentCompleted").disabled = false;
				$3("setMyPCResetImg").style.display = "";$3("setMyPCSaveImg").style.display = "";
			}
		}else{
			$3("setMyPCText").innerHTML = "不能设置进度";$3("percentCompleted").disabled = true;
			$3("setMyPCResetImg").style.display = "none";$3("setMyPCSaveImg").style.display = "none";
		}
	}
	
	function choseParentTaskDate(type){
		var date = "";
		if(type == 0){
			date = $("progressDateStringParent").value;
		}else{
			date = $("progressDateString").value;
		}
		getParentWHInit(date, type);
	}