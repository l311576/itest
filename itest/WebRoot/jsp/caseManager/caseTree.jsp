<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>

<html>
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<title>测试需求</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
	</head>
	<body bgcolor="#ffffff" leftmargin="0" topmargin="3" marginwidth="0" onload="javascript:parent.treeWin=window">
	<ww:hidden id="taskId" name="dto.taskId"></ww:hidden>
	<input type="hidden" id="currNodeId" name="currNodeId" />
	<input type="hidden" id="nodeDataStr" name="nodeDataStr" value="${nodeDataStr}"/>
	<div id="treeBox" style="width:100%;height:100%"></div> 
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/caseManager/caseTree.js"></script>
	<script type="text/javascript">
		popContextMenuFlg=0;
	 	parent.mypmLayout.items[1]._frame.contentWindow.tree= tree;
		parent.mypmLayout.items[1]._frame.contentWindow.treeWin = window;
		parent.treeDisModel = "${dto.command}";
	if(parent.treeDisModel=="")
		parent.treeDisModel="simple";
	if(parent.treeDisModel=="simple")
		parent.mypmLayout.items[0].setText("测试需求&nbsp;&nbsp;<a href='javascript:void(0);' onclick='swTreeDisModel()' ><font color='blue' size='2pt'>普通视图</font><font color='green' size='1pt'>(双击切换)</font></a>");
	else
		parent.mypmLayout.items[0].setText("测试需求&nbsp;&nbsp;<a href='javascript:void(0);' onclick='swTreeDisModel()' ><font color='blue' size='2pt'>Bug用例统计视图</font></a>");
	</script>
	</body>
</html>
