	var testFlowStr=$("testFlowStr").value;
	var initFlowStr=$("initFlowStr").value;
	function checkInit(){
		var flowStr = testFlowStr.split(",");
		for(var i=0 ;i<flowStr.length; i++){
			if(flowStr[i]==2){
				$("testerBugConfirm").checked =true ;
			}
			if(flowStr[i]==3){
				$("analyse").checked =true ;
			}
			if(flowStr[i]==4){
				$("assignation").checked =true ;
			}
			if(flowStr[i]==6){
				$("devConfirmFix").checked =true ;
			}
			if(flowStr[i]==7){
				$("arbitrateBug").checked =true ;
			}
			if(flowStr[i]==9){
				$("caseReview").checked =true ;
			}
			if(flowStr[i]==11){
				$("proRelaPersonFlg").checked =true ;
			}
		}
	}
	checkInit();
	function speUpSumbit(callback){
		var cb = callback;
		if(typeof cb != "undefined" &&(!cb)==true){
			return;
		}
		$("reportBug").checked=true;
		$("devFixBug").checked=true;
		$("arbitrateBug").checked=true;
		$("testerVerifyFix").checked=true;
		if($("mailOverdueBug").checked&&!$("proRelaPersonFlg").checked){
			hintMsg("因没勾选项目关注，通知项目关注人设置无法生效");
		}
		var url=conextPath+"/testTaskManager/testTaskManagerAction!update.action?dto.testFlowStr="+testFlowStr+"&dto.initFlowStr="+initFlowStr;
		var ajaxResut = dhtmlxAjax.postSync(url, "createForm").xmlDoc.responseText;
		if(ajaxResut == "success"){
			if(parent.pmGrid.getColumnsNum()>=18){//从任务列表过来
				parent.pmGrid.cells(parent.testTaskSelectedId,18).setValue(1);
				parent.testTaskSelectedId = "";
			}else{//流程设置列表过来的
				parent.pmGrid.cells($("taskId").value,10).setValue("进行中");
			}
			parent.testW_ch.setModal(false);
			parent.testW_ch.hide();
			initFlowStr=testFlowStr;
		}else{
			$("cUMTxt").innerHTML = "操作失败";
		}
		$("reportBug").disabled=true;
		$("devFixBug").disabled=true;
		$("testerVerifyFix").disabled=true;
	}
	function addUpCB(){  
		if($("testLeadId").value==""){
			hintMsg("请选择测试负责人");
			return false;
		}else if($("testerId").value==""){
			hintMsg("请选择测试人员");
			return false;
		}else if($("programmerId").value==""){
			hintMsg("请选择修改人");
			return false;
		}else if($("caseReview").checked==true&&$("caseReviewId").value==""){
			hintMsg("请选择用例Review人员");
			return false;
		}else if($("testerBugConfirm").checked==true&&$("testerConfId").value==""){
			hintMsg("请选择互验人员");
			return false;
		}else if($("analyse").checked==true&&$("analyserId").value==""){
			hintMsg("请选择分析人");
			return false;
		}else if($("assignation").checked==true&&$("assignationId").value==""){
			hintMsg("请选择分配人");
			return false;
		}else if($("devConfirmFix").checked==true&&$("empolderAffirmantId").value==""){
			hintMsg("请选择互检人");
			return false;
		}else if($("arbitrateBug").checked==true&&$("empolderLeader").value==""){
			hintMsg("请选择仲载人");
			return false;
		}else if($("proRelaPersonFlg").checked==true&&$("proRelaPersonId").value==""){
			hintMsg("请选择关注人");
			return false;
		}
		$("cUMTxt").innerHTML = "&nbsp;";
		$("reportBug").disabled=false;
		$("devFixBug").disabled=false;
		$("testerVerifyFix").disabled=false;
		return true;
	}
	function rewtFlowStr(){
			var form = $("createForm");
			var elements = form.elements;
			var element;
			var flowStr = "";
			for (var i = 0; i < elements.length; i++) {
			 	element = elements[i];
			 	if (element.type == "checkbox"&& (element.id.indexOf("mail")<0)){
			 		if(element.id.indexOf("rel")<0&&element.checked==true){
						flowStr = flowStr+","+element.value;
			 		}
			 	}
			}
			testFlowStr= flowStr.substring(1);
	}
	var pageUrl = conextPath+"/testTaskManager/testTaskManagerAction!softVerList.action";
	function pageAction2(pageNo, pSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var purl = pageUrl +"?dto.pageNo="+ pageNo +"&dto.pageSize=" + pSize;
		var ajaxRest = postSub(purl,"");
		$("listStr").value=ajaxRest;
		initGrid(pmGrid,"listStr");	
   		return;
	}
	function loadGrid(){
		if(haveLoadVerInfo==0){
			importJs(conextPath+"/dhtmlx/grid/codebase/dhtmlxgrid.js");
			importJs(conextPath+"/dhtmlx/grid/codebase/dhtmlxgridcell.js");
			pmGrid = new dhtmlXGridObject('gridbox');
			pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
		    pmGrid.setHeader("&nbsp;,序号,版本名称,顺序号,备注,状态");//最后一列是状态
		    pmGrid.setInitWidths("25,40,100,60,*,40");
		    pmGrid.setColAlign("center,center,left,left,left,left");
		    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
		    pmGrid.setColSorting("int,str,str,int,str,str");
			pmGrid.enableAutoHeight(true,500);
		    pmGrid.init();
		    pmGrid.enableRowsHover(true, "red");
		    pmGrid.enableTooltips("false,true,true,true,true");
		    pmGrid.setSkin("light");  
			loadCSS(conextPath+"/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css");
			importJs(conextPath+"/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js");
			pmBar = new dhtmlXToolbarObject("toolbarObj");
			pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
			pmBar.addButton("new", 1, "", "new.gif", "new.gif");
			pmBar.setItemToolTip("new", "新建版本");
			pmBar.addButton("update", 2, "", "iconForm.gif", "iconForm.gif");
			pmBar.setItemToolTip("update", "修改版本");
			pmBar.addButton("swSta", 3, "", "enable.png", "enable.png");
			pmBar.setItemToolTip("swSta", "启用");
			pmBar.addButton("del", 4, "", "iconDelete.gif", "iconDelete.gif");
			pmBar.setItemToolTip("del", "删除");
			pmBar.addButton("reFreshP",5 , "", "page_refresh.gif");
			pmBar.setItemToolTip("reFreshP", "刷新页面");
			pmBar.addButton("first", 6, "", "first.gif", "first.gif");
			pmBar.setItemToolTip("first", "第一页");
			pmBar.addButton("pervious",7, "", "pervious.gif", "pervious.gif");
			pmBar.setItemToolTip("pervious", "上一页");
			pmBar.addSlider("slider",8, 80, 1, 30, 1, "", "", "%v");
			pmBar.setItemToolTip("slider", "滚动条翻页");
			pmBar.addButton("next",9, "", "next.gif", "next.gif");
			pmBar.setItemToolTip("next", "下一页");
			pmBar.addButton("last",10, "", "last.gif", "last.gif");
			pmBar.setItemToolTip("last", "末页");
			pmBar.addInput("page",11, "", 25);
			pmBar.addText("pageMessage",12, "");
			pmBar.addText("pageSizeText",13, "每页");
			var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
			pmBar.addButtonSelect("pageP",14, "page", opts);
			pmBar.setItemToolTip("pageP", "每页记录数");
			pmBar.addText("pageSizeTextEnd",15, "条");
			$("toolbarObj").style.width = $("gridbox").style.width;
		    pmGrid.attachEvent("OnCheck",doOnCheck);
		    pmGrid.attachEvent("onRowSelect",doOnSelect);  
		    function doOnCheck(rowId,cellInd,state){
				this.setSelectedRow(rowId);
				return true;
			}
			pmBar.attachEvent("onEnter", function(id, value) {
				if(!isDigit(value, false)){
					pmBar.setValue("page", pageNo);
					return;
				}
				var pageNoTemp = parseInt(value);
				if(pageNoTemp < 1){
					pageNoTemp = 1;
				}else if(pageNoTemp > pageCount){
					pageNoTemp = pageCount;
				}
				pageAction2(pageNoTemp, pageSize);
				return;
			});
			pmBar.attachEvent("onClick", function(id) {
				if(id == "first"){
					pageNo = 1;
					pageAction2(pageNo, pageSize);
				}else if(id == "last"){
					pageNo = pageCount;
					pageAction2(pageNo, pageSize);
				}else if(id == "next"){
					pageNo = pageNo +1
					pageAction2(pageNo, pageSize);
				}else if(id == "pervious"){
					pageNo = pageNo -1
					pageAction2(pageNo, pageSize);
				}else if(id == "id1" || id == "id2" || id == "id3"){
					var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
					if(pageSize==pageSizeTemp){
						return;
					}
					pageSize = pageSizeTemp;
					pageAction2(pageNo, pageSize);
				}
			});
			pmBar.attachEvent("onClick", function(id) {
				if(id == "new"){
					addInit();
				}else if(id == 'update'){
					var verId = pmGrid.getSelectedId();
					if(verId==null){
						hintMsg("请选择要修改的记录");
						return;
					}
					upInit();
				}else if(id == 'del'){
					var verId = pmGrid.getSelectedId();
					if(verId==null){
						hintMsg("请选择要删除的记录");
						return;
					}
					cfDialog("delExe","您确定删除选择的记录?",false);
				}else if(id == 'swSta'){
					var verId = pmGrid.getSelectedId();
					if(verId==null){
						hintMsg("请选择要切换状态的记录");
						return;
					}
					cfDialog("swStaExe","您确定为所选记录切换状态?",false);
				}else if(id=="reFreshP"){
					pageAction2(pageNo, pageSize);
				}
			});
			pmBar.attachEvent("onEnter", function(id, value) {
				if(!isDigit(value, false)){
					pmBar.setValue("page", pageNo);
					return;
				}
				var pageNoTemp = parseInt(value);
				if(pageNoTemp < 1){
					pageNoTemp = 1;
				}else if(pageNoTemp > pageCount){
					pageNoTemp = pageCount;
				}
				pageAction2(pageNoTemp, pageSize);
				return;
			});
			pmBar.attachEvent("onValueChange", function(id, pageNo) {
				pageAction2(pageNo, pageSize);
				return;
			});
	    }
	    initGrid(pmGrid,"listStr");	
	}
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		var rest = pmGrid.cells(rowId, 5).getValue();
		if(rest=="启用"){
			pmBar.setItemImage("swSta", "disable.png");
			pmBar.setItemToolTip("swSta", "停用，停用后在选择版本时不列出该版本");
		}else{
			pmBar.setItemImage("swSta", "enable.png");
			pmBar.setItemToolTip("swSta", "启用，启用后在版本选择时列出该版本");
		}
		
	}
	function  delExe(){
		if(!canSet){
	 		hintMsg("您无流程设置权限");
	 		return;
	 	}
	 	if($("testTaskState").value=="1"||$("testTaskState").value=="2"){
	 		hintMsg("己完成或结束不能设置流程");
	 		return;	 	
	 	}
		var verId = pmGrid.getSelectedId();
		var url = conextPath+"/testTaskManager/testTaskManagerAction!delSoftVer.action?dto.softVer.versionId="+verId;
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="success"){
			pmGrid.deleteRow(verId);
			clsoseCfWin();
		}else
			hintMsg("删除发生错误");	
	}
	function swStaExe(){
		if(!canSet){
	 		hintMsg("您无流程设置权限");
	 		return;
	 	}
	 	if($("testTaskState").value=="1"||$("testTaskState").value=="2"){
	 		hintMsg("己完成或结束不能设置流程");
	 		return;	 	
	 	}
		var verId = pmGrid.getSelectedId();
		var url = conextPath+"/testTaskManager/testTaskManagerAction!softVerSwStatus.action?dto.softVer.versionId="+verId;
		var swStatus = "";
		if(pmGrid.cells(verId,5).getValue()=="启用"){
			swStatus = "停用";
			url+="&dto.softVer.verStatus=1";
		}else{
		   swStatus = "启用";
		   url+="&dto.softVer.verStatus=0";
		 }
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="success"){
			pmGrid.cells(verId,5).setValue(swStatus);
			clsoseCfWin();
		}else
			hintMsg("切换状态发生错误");
	}
	var cVerW_ch;
	function addInit(){
		if(!canSet){
	 		hintMsg("您不是测试负责人,无流程设置权限");
	 		return;
	 	}
	 	if($("testTaskState").value=="1"||$("testTaskState").value=="2"){
	 		hintMsg("己完成或结束不能设置流程");
	 		return;	 	
	 	}
		$("versionId").value="";
		$("verStatus").value="0";
		$("verCForm").reset();
		$("saveVc_b").style.display="";
		$("cUMTxt2").innerHTML="";
		cVerW_ch =initW_ch(cVerW_ch, "createDiv", true, 360, 115, "", "新建版本");
		cVerW_ch.show();
		cVerW_ch.bringToTop();
		var verId = pmGrid.getSelectedId();
		
	}
	function upInit(){
		if(!canSet){
	 		hintMsg("您无流程设置权限");
	 		return;
	 	}
	 	if($("testTaskState").value=="1"||$("testTaskState").value=="2"){
	 		hintMsg("己完成或结束不能设置流程");
	 		return;	 	
	 	}
		$("verCForm").reset();
		$("saveVc_b").style.display="none";
		var verId = pmGrid.getSelectedId();
		$("versionId").value=verId;
		$("cUMTxt2").innerHTML="";
		$("versionNum").value=pmGrid.cells(verId,2).getValue();
		$("verSeq").value=pmGrid.cells(verId,3).getValue();
		$("remark").value=pmGrid.cells(verId,4).getValue();
		if(pmGrid.cells(verId,5).getValue()=="户用")
			$("verStatus").value="0";
		else
		    $("verStatus").value="1";
		cVerW_ch =initW_ch(cVerW_ch, "createDiv", true, 360, 115, "", "修改版本");
		cVerW_ch.show();
		cVerW_ch.bringToTop();
	}
	function addUpSub(conFlg){
		if(!subChk())
			return;
		var url = conextPath+"/testTaskManager/testTaskManagerAction!addSoftVer.action";
		if($("versionId").value!="")
			url = conextPath+"/testTaskManager/testTaskManagerAction!udateSoftVer.action";
		var ajaxRest = postSub(url,"verCForm");
		if(ajaxRest.indexOf("success")>=0){
			var rowData = "0,,"+$("versionNum").value +","+$("verSeq").value+","+$("remark").value+",启用";
			var rowId = $("versionId").value;
			if(rowId!=""){
				pmGrid.deleteRow(rowId);
				pmGrid.addRow2(rowId,rowData,0);		
			}else{
				rowId = ajaxRest.split("^")[1];
				pmGrid.addRow2(rowId,rowData,0);
			}
			setRowNum(pmGrid);
			if(typeof(conFlg)!="undefined"&&rowId!=""){
				$("versionId").value="";
				$("verCForm").reset();			
			}else{
				cVerW_ch.setModal(false);
				cVerW_ch.hide();			
			}
			$("cUMTxt2").innerHTML="操作成功";
		}else if(ajaxRest=="failed"){
			$("cUMTxt2").innerHTML="保存版本信息发生错误";
		}else if(ajaxRest=="numRep"){
			$("cUMTxt2").innerHTML="名称重复";
		}else if(ajaxRest=="seqRep"){
			$("cUMTxt2").innerHTML="顺序号重复"
		}else if(ajaxRest=="numRepseqRep"){
			$("cUMTxt2").innerHTML="名称及顺序号重复"
		}
	}
	function subChk(){
		if(isWhitespace($("versionNum").value)){
			$("cUMTxt2").innerHTML="请填写版本名称";
			return false;
		}else if(includeSpeChar($("versionNum").value)){
			$("cUMTxt2").innerHTML="版本名称不能含特殊字符";
			return false;		
		}else if(checkIsOverLong($("versionNum").value,32)){
			$("cUMTxt2").innerHTML="版本名称超过32位";
			return false;
		}else if(isWhitespace($("verSeq").value)){
			$("cUMTxt2").innerHTML="顺序号不能为空";
			return false;
		}else if(!isDigit($("verSeq").value, false)){
			$("cUMTxt2").innerHTML="顺序号只能是数字";
			return false;
		}else if(checkIsOverLong($("remark").value,100)){
			$("cUMTxt2").innerHTML="备注超过100位";
			return false;
		} 
		return true;
	}
  function includeSpeChar(StrVal){
  	var speStr="~ ` ! # $ % ^ & * ( ) [ ] { } ; ' : \" , ， < >";
  	var speStrArr = speStr.split(" ");
  	if(typeof(StrVal)=="undefined"||isWhitespace(StrVal))
  		return false;
  	for(var i=0; i<speStrArr.length; i++){
  		if(StrVal.indexOf(speStrArr[i])>=0){
  			return true;
  		}
  	}
  	return false;
  }