	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
 
	function initW_ch(obj, divId, mode, w, h,wId){
		if((typeof obj != "undefined")&& !obj.isHidden()){
			obj.setDimension(w, h);
			obj.centerOnScreen();
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
			obj.centerOnScreen();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			obj.centerOnScreen();
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}
	var popW_ch;
	var popGrid;
	var tyId,tpNa;
	var sTime =0;
	function popSelWin(passValId,passNameId,dataId,title){
		if(passValId==tyId&&((new Date()).getTime()-sTime)<1000){
			return;
		}
		sTime = (new Date()).getTime();
		tyId = passValId;
		tpNa = passNameId;
		if(typeof treeW_ch != "undefined"&&treeW_ch.isModal()){
			treeW_ch.setModal(false);
		}
		if(tyId=="currQueryId"&&$("queryCount").value>0&&$("querySelStr").value==""){
			var url = conextPath+ "/bugManager/bugManagerAction!loadQueryList.action?dto.isAjax=true"; 
			var ajaxResut = postSub(url,"");
			if(ajaxResut!="failed"){
				$("querySelStr").value=ajaxResut;
			}else{
				hintMsg("初始化查询器失败");
				return;
			}
		}
		if(typeof popW_ch == "undefined"){
			popW_ch = initW_ch(popW_ch, "", true, 300, 230,'popW_ch');
			popGrid = popW_ch.attachGrid()
			popGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			popGrid.setHeader(",,单击选择");
			popGrid.setInitWidths("0,0,*");
	    	popGrid.setColAlign("left,left,left");
	    	popGrid.setColTypes("ch,ro,ro");
	   		popGrid.setColSorting("Str,int,str");
	   		popGrid.enableTooltips("false,false,true");
	    	popGrid.enableAutoHeight(true, 180);
	    	popGrid.enableRowsHover(true, "red");
	        popGrid.init();
	    	popGrid.setSkin("light");
		    popGrid.attachEvent("onRowSelect",function (rowId,index){
		    	if(rowId!="-1"){
		    		$(tyId).value=rowId;
		    		if(tyId=="testOwnerId"||tyId=="analyseOwnerId"||tyId=="assinOwnerId"||tyId=="devOwnerId"){
		    			$("nextOwnerId").value=rowId;
		    		}
					nextOwnerIdV = "";	
		    		var vName = popGrid.cells(rowId,2).getValue();
		    		if(tyId=="currQueryId"&&vName.lastIndexOf("[")>0&&vName.lastIndexOf("]")>0){
		    			$(tpNa).value = vName.substring(0,vName.lastIndexOf("["));
		    		}else{
		    			$(tpNa).value = vName;
		    		}
		    	}else{
		    		$(tpNa).value="";
		    		$(tyId).value=""
		    	}
	   			if("bugFreqId"==tyId){
	   				if($(tpNa).value=="偶尔出现"){
	   					$("repropTd").style.display="";
						$("reprop").style.display="";	   				
	   				}else{
	   					$("repropTd").style.display="none";
						$("reprop").style.display="none";
					}
	   			}else if("bugOccaId"==tyId){
	   				if( $(tpNa).value=="首次出现"){
	   					$("geneCauseTd").style.display="";
						$("geneCauseF").style.display="";	   				
	   				}else{
	   					$("geneCauseTd").style.display="none";
						$("geneCauseF").style.display="none";	   				
	   				}
	   			}else if(tyId=="currQueryId"&&rowId!="-1"){
	   				$("queryBtnTd").style.display="";
	   				$("newQueryBtn").style.display="none";
	   				$("saveQueryN").checked=false;
	   				$("upQuBtn").style.display="none";
	   			}
				popW_ch.hide();
				popW_ch.setModal(false);
			});
		}
		popW_ch.setText(title);
		popW_ch.center();
		popGrid.clearAll();
		var selData = $(dataId).value;
		if(selData!=""){
			selDatas = selData.split("$");
			for(var i = 0; i < selDatas.length; i++){
				var items = selDatas[i].split(";")
				popGrid.addRow(items[0],"0,,"+items[1],i);
			}
			if(opreType=="query"&&tyId!="currQueryId"){
				popGrid.addRow("-1","0,,----所有------",0);
			}
			if($(tyId).value!=""&&popGrid.getRowIndex($(tyId).value)>=0){
				popGrid.setRowColor($(tyId).value, "#CCCCCC");
			}else if(opreType=="query"&&tyId!="currQueryId"){
				popGrid.setRowColor("-1", "#CCCCCC");
			}
		}
		if(tyId=="currQueryId"&&selData==""){
			popGrid.addRow('-1',"0,,您此前没有保存任何查询为查询器!请填写查询条件查询");
		}
		cuW_ch.setModal(false);
		popW_ch.setModal(true);
		popW_ch.show();
		popW_ch.bringToTop();
		return;
	}
	function getMdPerson(loadType){
		if($("bugModuleId").value==""){
			hintMsg('当前没指定对应的测试需求');
			return;
		}
		var url = conextPath + "/bugManager/bugManagerAction!loadMdPerson.action?dto.currNodeId="+$("bugModuleId").value+"&dto.loadType="+loadType;
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			if(loadType=="1")
				hintMsg('初始化测试需求对应开发人员失败');
			else 
			    hintMsg('初始化测试需求对应分配人员失败');
			return;
		}else if(ajaxRest==""){
			if(loadType=="1")
				hintMsg('当前测试需求没指定开发人员');
			else
			    hintMsg('当前测试需求没指定分配人员');
			return;		
		}
		$("moduleDevStr").value=ajaxRest;
		if(loadType=="1")
			popSelWin('devOwnerId','devOwnerName','moduleDevStr','修改人');
		else
		   popSelWin('assinOwnerId','anasnOwnerName','moduleDevStr','分配人');
	}

	function disReset(){
		$("impPhaTdtxt").style.display="none";
		$("impPhaTd").style.display="none";
		$("repropTd").style.display="none";
		$("reprop").style.display="none";
		$("geneCauseTd").style.display="none";
		$("geneCauseF").style.display="none";
		$("testOwnerTr").style.display="none";
		$("analOwnerTr").style.display="none";
		$("anasnOwnerTr").style.display="none";
		$("devOwnerTr").style.display="none";
		$("intecsOwnerTr").style.display="none";
		$("assFromMdTd").style.display="none";	
		$("anaAssFromMdTd").style.display="none";	
	}
	function resetHdeField(){
		$("moduleId").value="";
		$("bugTypeId").value="";
		$("bugGradeId").value="";
		$("bugFreqId").value="";
		$("bugOccaId").value="";
		$("priId").value="";
		$("genePhaseId").value="";
		$("geneCauseId").value="";
		$("sourceId").value="";
		$("platformId").value="";
		$("devOwnerId").value="";
		$("testOwnerId").value="";
		$("analyseOwnerId").value="";
		$("assinOwnerId").value="";
		$("intercessOwnerId").value="";
		$("platformId").value="";
		$("cUMTxt").innerHTML = "";
		$("bugReptVer").value="";
		if(typeof oEditor != "undefined"){
			oEditor.SetData("");
		}
	}
	function setInitInfo(updInfo){
		if(updInfo==""){
			return;
		}
		var updInfos = updInfo.split("^");
		var valueStr = "";
		var selStr = "";
		var selId = ""
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				eval(valueStr);	
				if(currInfo[0]=="ReProStep"){
					$("initReProStep").value=currInfo[1];
				}
				if(currInfo[0]=="nextFlowCd"){
					flowControl_new(currInfo[1]);
				}
				if(currInfo[0]=="testFlow"){
					flwStr=currInfo[1];
				}
				if(currInfo[0]=="roleInTask"){
					roleStr =currInfo[1];
				}			
			}
		}
		return;
	}	
	function flowControl_new(nextFlowCd){
		if(nextFlowCd=="2"){
			$("testOwnerTr").style.display="";
		}else if(nextFlowCd=="3"){
			$("analOwnerTr").style.display="";
		}else if(nextFlowCd=="4"){
			$("anasnOwnerTr").style.display="";
			$("anaAssFromMdTd").style.display="";	
		}else if(nextFlowCd=="5"){
			$("devOwnerTr").style.display="";
			$("assFromMdTd").style.display="";
		}else if(nextFlowCd=="7"){
			$("intecsOwnerTr").style.display="";
		}
	}
	function setBaseInfo(baseInfo){ 
		if(baseInfo==""){
			return;
		}
		var updInfos = baseInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				if(currInfo[0]=="typeSelStr"||currInfo[0]=="priSelStr"){
					var selStr = currInfo[1];
					var selId="priId";
					if(currInfo[0]=="typeSelStr"){
						selId="caseTypeId";
					}
					loadSel(selId,selStr);	
				}else{
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
					eval(valueStr);	
					if(currInfo[0]=="expResult"){
						$("expResultOld").value=currInfo[1];
					}
					if(currInfo[0]=="operDataRichText"){
						$("initOpd").value=currInfo[1];
					}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
						$("currAttach2").style.display="";
			 			$("currAttach2").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);			
					}				
				}
			}
		}
	}
	function loadSel(selId,selStr,splStr){
		if(selStr==""){
			return;
		}
		$(selId).options.length = 1;
		var options ;
		if(splStr){
			options = selStr.split(splStr);
		}else{
			options = selStr.split("$");
		}
		for(var i = 0; i < options.length; i++){
			var optionArr = options[i].split(";");
			if(optionArr[0] != "")
				var selvalue = optionArr[0] ;
				var selable = optionArr[1];
				$(selId).options.add(new Option(selable,selvalue));
		}	
	}
	var detlW_ch;
	function viewDetl(){
		detlW_ch = initW_ch(detlW_ch, "", true, 800, 520,'detlW_ch');
		var url= conextPath +"/bugManager/bugManagerAction!viewBugDetal.action";
		url+="?dto.bug.bugId="+pmGrid.getSelectedId();;
		detlW_ch.attachURL(url);	
	    detlW_ch.show();
	    detlW_ch.bringToTop();
	    detlW_ch.setModal(true);
	    detlW_ch.setText("明细");
	}
	function relaBug(){
		if(!canExe){
			hintMsg("您没有执行用例的权限");
			return;		
		}
		if($("bugIds").value.replace(/\s+$|^\s+/g,"")==""&&$("saveRela").style.display==""){
			hintMsg("请选择要关联的BUG");
			return;
		}
		if($("bugIds").value==bugIdsInit&&$("upRela").style.display==""){
			parent.relBugW_ch.setModal(false);
			parent.relBugW_ch.hide();	
			return;	
		}
		var currCaseTestRedSet = "";
		if(isExeRela==1){
			currCaseTestRedSet = parent.getCurCaseExeRecd();
		}
		var url= conextPath+"/bugManager/relaCaseAction!caseRelaBug.action?dto.testCaseId="+$("testCaseId").value;
		url+="&dto.bugIds="+$("bugIds").value+"&dto.moduleId="+$("moduleIdTmp").value+"&dto.isExeRela="+isExeRela;
		if(currCaseTestRedSet!=""){
			url+="&dto.currCaseTestRedSet="+currCaseTestRedSet;
		}
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg("关联BUG发生错误");
			return;
		}
		parent.relBugW_ch.setModal(false);
		parent.relBugW_ch.hide();
		if(isExeRela=="1"){
			parent.upGridExeRecord(ajaxResut);
		}
	}	
	

	var contin ="";
	function newSub(){
		if(addSubCheck()){
			$("bugModuleId").value=$("moduleIdTmp").value
			var url = conextPath + "/bugManager/bugManagerAction!add.action";
			$("moduleNames").value=$("mdPath").value;
			var ajaxResut = postSub(url,"createForm");
			if(ajaxResut.indexOf("^") != -1){
				ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
				var result = ajaxResut.split("^");
				if(result[0] == "success"){
					reSetFlg=1;
					eraseAttach();
					colTypeReset();
					//alert(result[2]);
					pmGrid.addRow(result[1],result[2],0);
					//pmGrid.cells(result[1],9).setValue(myName);
					//setRowNum(pmGrid);
					loadLink();
					//sw2Link();
					pmGrid.setSelectedRow(result[1]);	
					pmGrid.setSizes();	
					var currStateId=$("currStateId").value,stateName=$("stateName").value;
					var nextFlowCd=$("nextFlowCd").value;
					if(getCookie("clearFlg")==0){
						oEditor.SetData("<strong>"+$("mdPath").value +"</strong>: ");
						$("bugDesc").value="";			
					}else{
						resetHdeField();
						$("createForm").reset();
						$("clearUpFlg").checked=false;
						disReset();
					}
					upVarReset();
					contin !=""
					$('upStatusBar').innerHTML="";
					if(contin !=""){
						$("cUMTxt").innerHTML =  "操作成功";
						$("bugDesc").focus();
						flowControl_new(nextFlowCd);
						$("currStateId").value=currStateId;
						$("stateName").value=stateName;
						$("nextFlowCd").value=nextFlowCd;
						oEditor.SetData("<strong>"+$("mdPath").value +"</strong>:");
						upVarReset();
					}else{
						cuW_ch.setModal(false);
						cuW_ch.hide();
						upVarReset();
					}
					return;					
				}else{
					$("cUMTxt").innerHTML =  "操作失败";
					contin !=""
				}
			}else{
				$("cUMTxt").innerHTML =  "操作失败";
				contin !=""
			}
		}
	}
	function addSubCheck(){
		if($("bugReptVer").value==""){
			hintMsg("请选择发现版本");
			return false;		
		}else if(isAllNull($("bugDesc").value)){
			hintMsg("请填写问题描述");
			$("bugDesc").focus();
			return false;
		}else if($("bugTypeId").value==""){
			hintMsg("请选择类型");
			return false;
		}else if($("bugGradeId").value==""){
			hintMsg("请选择等级");
			return false;
		}else if($("platformId").value==""){
			hintMsg("请选择平台");
			return false;
		}else if($("sourceId").value==""){
			hintMsg("请选择来源");
			return false;
		}else if($("bugOccaId").value==""){
			hintMsg("请选择发现时机");
			return false;
		}else if($("geneCauseF").style.display==""&& $("geneCauseId").value==""){
			hintMsg("请选测试时机");
			return false;		
		}else if($("bugFreqId").value==""){
			hintMsg("请选择频率");
			return false;
		}else if($("reprop").style.display==""&&$("reproPersent").value==""){
			hintMsg("请填写再现比例");
			$("reprop").focus();
			return false;		
		}else if($("nextFlowCd").value=="2"&&$("testOwnerId").value==""){
			hintMsg("请选择互验人");
			return false;
		}else if($("nextFlowCd").value=="3"&&$("analyseOwnerId").value==""){
			hintMsg("请选择分析人");
			return false;
		}else if($("nextFlowCd").value=="4"&&$("assinOwnerId").value==""){
			hintMsg("请选择分配人");
			return false;
		}else if($("nextFlowCd").value=="5"&&$("devOwnerId").value==""){
			hintMsg("请选择修改人");
			return false;
		}else{
			$("reProStep").value=oEditor.GetXHTML().replace(/\s+$|^\s+/g,"");
			if($("reProStep").value=="<br />"||$("reProStep").value==""||$("reProStep").value=="<strong><br></strong>"){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			for(var i=1; i<31;i++){
				$("reProStep").value = $("reProStep").value.trim("<br>");
				$("reProStep").value = $("reProStep").value.trim("&nbsp;");	
				$("reProStep").value = $("reProStep").value.replace(/\s+$|^\s+/g,"");	
				$("reProStep").value = $("reProStep").value.trim("\\");		
			}
			if(checkIsOverLong($("reProStep").value,1000)){
				hintMsg("再现步骤不能超过1000个字符");
				return false;			
			}
			var repTxt = $("reProStep").value.replace(/\s+$|^\s+/g,"");
			var mdPath = "<strong>"+$("mdPath").value +"</strong>:";
			if(repTxt==mdPath){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			$("reProTxt").value=html2PlainText('reProStep');
		}
		if(checkIsOverLong($("bugDesc").value,150)){
			hintMsg("描述不能超过150个字符");
			return false;			
		}
		$("bugDesc").value = $("bugDesc").value.replace(/\s+$|^\s+/g,"");
		return true;
	}
	function setCookie(obj, time){
		var date=new Date();
		date.setTime(date.getTime()+ 2592000000);
		document.cookie = obj + "; expires=" + date.toGMTString();
	}
		
	function getCookie(key){
		var strCookie = document.cookie;
		var arrCookie = strCookie.split("; ");
		var value = "";
		for(var i = 0; i < arrCookie.length; i++){
			var arr = arrCookie[i].split("=");
			if(key == arr[0]){
				value = arr[1];		
				break;
			}
		}
		return value;
	}
	function setClearFlg(chkObj){
		if(!chkObj.checked){
			setCookie("clearFlg=1", 2592000000);			
		}else{
			setCookie("clearFlg=0", 2592000000);
		}
	}