	
	function popselWin(){
		suW_ch = initW_ch(suW_ch, "selPeopleDiv", true, 190, 380);
		suW_ch.setText("人员分配");
		initSelGrid();
		suW_ch.bringToTop();
	}
	var selGrid;
	function initSelGrid(){
	    if(typeof selGrid == "undefined"){
		    selGrid = new dhtmlXGridObject('selGridbox');
			selGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			selGrid.setHeader(",,<div title='双击数据行选择'>备选人员 -- 双击分配</div>");
			selGrid.setInitWidths("0,0,*");
	    	selGrid.setColAlign("left,left,left");
	    	selGrid.setColTypes("ro,ro,ro");
	   		selGrid.setColSorting("str,int,str");
	   		selGrid.enableTooltips("false,false,true");
	    	selGrid.enableAutoHeight(true, 240);
	    	selGrid.setMultiselect(true);
	        selGrid.init();
	        selGrid.enableRowsHover(true, "red");
	    	selGrid.setSkin("light");
	    	selGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
	    		assignPeople(rowId);
			});
			loadSelUser();
	    }
	}
	function loadSelUser(){
		var url = conextPath+"/outLineManager/outLineAction!selectMember.action?dto.taskId="+$("customSprHomeTaskId").value+"&dto.reqType=5";
		var ajaxResut  = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		if(ajaxResut=="failed"){
  			if(typeof hintMsg!="undefined"){
  				hintMsg("加载数据发生错误");
  			}
  			return;
  		}
		selGrid.clearAll();
		ajaxResut = ajaxResut.split("$")[1];
	    if(ajaxResut != ""&&ajaxResut!="failed"){
   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
  			selGrid.parse(eval("(" + ajaxResut +")"), "json");
  		}
		return;
	}
	
	function getFindInfo(){
	      	var form = document.getElementById("findForm");
			var elements = form.elements;
			var element;
			var findData = "";
			var l=0;
			for (i = 0; i < elements.length; i++) {
			 	element = elements[i];
			 	if (element.id == "bugDescF")
			 		$("bugDescA").value=element.value;
			 	if (element.id == "defBugId")	
			 		$("defBugIdA").value=element.value;
			 	if (element.id == "relaCaseFlagF")	
			 		$("relaCaseFlagA").value=element.value;			 		
			 	if (element.type == "hidden" &&element.name.indexOf("dto.bug")>=0){
			 		if(l==0)
			 			findData = element.name+"="+element.value;
			 		else
			 			findData = findData+"&"+element.name+"="+element.value;
			 		l++;
			 	}
			}
			return findData ;
	}
	function assignPeople(rowId){
		var bugIds = getChecked();
		if(bugIds==""){
			hintMsg("请选择要分配的BUG");
			return;
		}
		$("currOwner").value = selGrid.getSelectedId();
		$("currProjectId").value = bugIds;
		$("moduleNameA").value=selGrid.cells(rowId,2).getValue();
		var url = conextPath+"/bugManager/bugManagerAction!batchAssignExe.action?dto.pageSize="+pageSize;
		var findData =getFindInfo();
		if(findData!="")
			url = url +"&"+findData;
		var ajaxResut  =postSub(url, "assignForm");
		if(ajaxResut.indexOf("sucessAssign_")==0){
			var datas = ajaxResut.split("sucessAssign_");
			$("listStr").value=datas[1];
			pmGrid.clearAll();
			colTypeReset();
			initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
	   		loadLink();
			sw2Link();			
			suW_ch.hide();
			suW_ch.setModal(false);
			return true;
		}else if(ajaxResut=="failed"){
			hintMsg("分配人员发生错误");
		}
	}