<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>

<html>
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<title>测试需求</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
	</head>
	<body bgcolor="#ffffff" leftmargin="0" topmargin="3" marginwidth="0" onload="javascript:parent.treeWin=window">
	<ww:hidden id="taskId" name="dto.taskId"></ww:hidden>
	<input type="hidden" id="currNodeId" name="currNodeId" />
	<input type="hidden" id="nodeDataStr" name="nodeDataStr" value="${nodeDataStr}"/>
	<div id="treeBox" style="width:100%;height:100%"></div> 
	<script type="text/javascript" src="<%=request.getContextPath()%>//jsp/bugManager/bugMgrLeftTre.js"></script>
	<script type="text/javascript">
		popContextMenuFlg=0;
	 	parent.mypmLayout.items[1]._frame.contentWindow.tree= tree;
		parent.mypmLayout.items[1]._frame.contentWindow.treeWin = window;
	</script>
	</body>
</html>
