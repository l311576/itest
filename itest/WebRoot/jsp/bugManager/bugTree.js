	var rootId = "";
	popContextMenuFlg=0;
	tree=new dhtmlXTreeObject('treeBox',"100%","100%",0); 
	tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	tree.enableTreeLines(true);
	tree.enableHighlighting(1);
	tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	tree.setStdImages("book.gif","books_open.gif","books_close.gif");
	var mytreeArr = $("nodeDataStr").value.split(";");
	var topNode = "";
	for(var i=0 ;i<mytreeArr.length;i++){
		var nodeInfo = mytreeArr[i].split(",");
		tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
		if(nodeInfo[3]=="0"&& i>0){
			tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
			tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
			tree.closeItem(nodeInfo[1]);
		}else if(i==0){
			rootId = nodeInfo[1];
		}
	}	
	tree.selectItem(rootId,false,false);
	$("currNodeId").value=rootId;
	tree.setOnDblClickHandler(dbClickHdl);
	tree.setOnOpenEndHandler(onopenEndHdl);
	var nameIncPath;
	function dbClickHdl(id){
		if(tree.getParentId(id)==0){
			return true;
		}
		//放开只有叶子节点可写BUG的限制 2011-03017
		//if(tree.hasChildren(id)==0){
			var bugEditor;
			nameIncPath = getMyCovPath(id);
			if(parent.opreType=="update"){
				bugEditor = parent.editWin.oEditor;
				parent.editWin.$("moduleName").value=nameIncPath;
				parent.editWin.$("moduleId").value=id;
			}else if(parent.opreType=="bugHand"){
				parent.handWin.$("moduleName").value=nameIncPath;
				parent.handWin.$("moduleId").value=id;
				parent.treeW_ch.setModal(false);
				parent.treeW_ch.hide();
				return;
			}else{
				if(parent.opreType=="add"){
					parent.$("moduleId").value=id;
					parent.$("moduleName").value=nameIncPath;
					parent.$("assFromMdTd").style.display="";
					parent.$("bugDesc").focus();
					try{
						parent.$("moduleIdTaskId").value = $("taskId").value;
					}catch(err)	{}		
				}else{
					parent.$("mdBtn").style.display="";
					parent.$("moduleIdF").value=id;
					parent.$("moduleNameF").value=nameIncPath;
					parent.$("bugDescF").focus();
				}
				bugEditor = parent.oEditor;
			}
			parent.treeW_ch.setModal(false);
			parent.treeW_ch.hide();
			if(parent.opreType=="add"){
				var txt =bugEditor.GetData(false);
				if(txt.replace(/\s+$|^\s+/g,"")=="<br />"||txt==""||txt=="<strong><br /></strong>"){
					bugEditor.SetData("<strong>"+nameIncPath +"</strong>: ");
				}
			}
			return false;	
		//}
		//else{
		//	return true;
		//}
	}
	function appendNode(treeArr){
		var topNode = "";
		for(var i=0 ;i<treeArr.length;i++){
			var nodeInfo = treeArr[i].split(",");
			topNode = nodeInfo[0];
			if(i==0){
				var blankId = tree.getUserData(nodeInfo[0],"blankCh");
				var haveBlank = typeof blankId=="undefined" ;
				if(!haveBlank){
					tree.deleteItem(blankId,false);
					tree.setUserData(topNode,"blankCh");
				}
			}
			tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
			tree.setUserData(nodeInfo[1],"MyState",nodeInfo[4]);
			if(nodeInfo[4]==1){
				tree.setItemColor(nodeInfo[1],'#ff0000','#ff0000');
			}
			if(nodeInfo[3]=="0"){
				tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
				tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
				tree.closeItem(nodeInfo[1]);
			}
		}
	}
	function loadChildren(itemId){
		var url = conextPath+"/bugManager/bugManagerAction!loadTree.action";
		url = url+"?dto.taskId="+$("taskId").value +"&dto.isAjax=true&dto.currNodeId="+itemId;
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"&&ajaxResut!=""){
			var treeArr = ajaxResut.split(";");
			appendNode(treeArr);
		}
		if(ajaxResut==""){
			var blankId = tree.getUserData(itemId,"blankCh");
			tree.deleteItem(blankId,true);
			tree.setUserData(itemId,"blankCh");
		}				
	}
	function onopenEndHdl(id,mode){
		if(mode<0){
			return ;
		}
		var blankId = tree.getUserData(id,"blankCh");
		var haveBlank = typeof blankId=="undefined" ;
		if(!haveBlank){
			loadChildren(id);
		}
	}
	function getAllParent(itemId,p){
		pId = tree.getParentId(itemId);
		if(pId==0){
			return p;
		}else{
			p+= ","+pId +getAllParent(pId,p)	
		}
		return p;
	}
	function getMyCovPath(id){
		var ids= id+getAllParent(id,"");
		var idArr=ids.split(",");
		var texts = "";
		for(var i=(idArr.length-1);i>=0;i--){
			texts= texts +"/"+tree.getItemText(idArr[i])
		}
		return texts.substring(1);
	}	
	parent.treeLoad="ok";