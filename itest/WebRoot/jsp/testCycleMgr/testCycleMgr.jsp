<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>

<HTML>
	<HEAD>
		<TITLE>测试任务列表</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	    <script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
	<style type="text/css">
	 .prog-border {
	 height: 15px;
	 width: 150px;
	 background: #fff;
	 border: 1px solid #000;
	 margin: 0;
	 padding: 0;
	 }
	 .prog-bar {
	 height: 11px;
	 margin: 2px;
	 padding: 0px;
	 background: #178399;
	 font-size: 10pt;
	 }
	</style>	
	</HEAD>

	<BODY bgcolor="#ffffff" style="overflow-x:hidden;">
		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="toolbarObj" width="100%"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox" height="1000" width="100%"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr"value="${dto.listStr}" />
	<script type="text/javascript">
	ininPage("toolbarObj", "gridbox", 560);
	var myName = "${session.currentUser.userInfo.uniqueName}";
	<pmTag:button page="testCycle" find="true" checkAll="false" />
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,任务名,类型,状态,执行人,执行环境,计划开始日期,计划结束日期,备注");
    pmGrid.setInitWidths("40,40,*,50,50,100,80,80,80,40");
    pmGrid.setColAlign("center,center,left,left,left,left,center,center,center,center");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str,str,str,str");
	pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");	
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
	pmBar.attachEvent("onClick", function(id) {
		if(id=="sw2TaskAction"){
			openSwTaskList();
		}else if(id=="allTask"){
			var url = conextPath+ "/bugManager/bugManagerAction!sw2AllMyBug.action?dto.allTestTask=true&dto.defBug=1";
			window.location=url;
		}else if(id=="reFreshP"){
			pageAction(pageNo, pageSize);
		}else if(id=="back"){
			var reUrl= document.referrer;
			if(reUrl.indexOf("loadCaseBoard")>0){
				reUrl = conextPath+ "/caseManager/caseManagerAction!index.action?";
			}
			if(reUrl.indexOf("loadBugBoard")>0||reUrl.indexOf("/main.jsp")>0){
				reUrl = conextPath+ "/bugManager/bugManagerAction!loadAllMyBug.action?dto.allTestTask=true";
			}
			parent.mypmMain.location=reUrl;
		}
	});
	var myId = "${session.currentUser.userInfo.id}";
	var swWin;
	function openSwTaskList(){
		if(typeof(swWin)!="undefined" &&swWin.isHidden()){
	    	swWin.show();
	    	swWin.bringToTop();
	    	swWin.setModal(true);
	    	return;			
		}
		swWin = initW_ch(swWin, "", true, 890, 500,'swWin');
		swWin.attachURL(conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=testCyleList");
		swWin.setText("请选择测试项目");	
    	swWin.show();
    	swWin.bringToTop();
    	swWin.setModal(true);			
	}
	
	
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		try{
			if(pmGrid.cells(rowId,4).getValue()=="己生效"){
			  pmBar.disableItem("update");
			}else{
				pmBar.enableItem("update");
			}
		}catch(err){
		}
		return true;
	}
    
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		try{
			if(pmGrid.cells(rowId,4).getValue()=="己生效"){
			  pmBar.disableItem("update");
			}else{
				pmBar.enableItem("update");
			}
		}catch(err){
		}
	}
	function colTypeReset(){
		pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ra,ro,link,ro,ro,ro,ro,ro,ro,ro");
	}	
	loadLink();	
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,2).cell.innerHTML="<a href='javascript:viewDetl()' title='查看详情'>"+getTttle2(i,2)+"</a>";
			if(getTttle2(i,9)!=""&&getTttle2(i,9).indexOf("src")<0){
				pmGrid.cells2(i,9).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='打开附件' onclick=\"openAtta('"+getTttle2(i,9)+"')\"/>";
			}		
		}
		sw2Link();
	} 
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	} 
	</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			 <ww:hidden id="sendName" name="sendName"></ww:hidden>
			<table id="createTable" class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			<ww:form theme="simple" method="post" id="createForm"name="createForm" namespace="" action="">
			<ww:hidden id="testCycleTaskId" name="dto.testCycleTask.id"></ww:hidden>
			<ww:hidden id="attachUrl" name="dto.testCycleTaskId.attachUrl"></ww:hidden>
			<tr class="ev_mypm" >
			  <td width="600" colspan="4" >
			  </td>
			</tr>
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" style="color:red;">
			  	 测试包名称:
			  	 </td>
			    <td class="dataM_left"  colspan="3">
					<ww:textfield id="name" name="dto.testCycleTask.name" cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="ev_mypm">
			  	 <td class="rightM_center" style="color:red;">
			  	 包类型: 
			  	 </td>
			    <td class="dataM_left">
						<ww:select id="cycleType" name="dto.testCycleTask.cycleType"
						list="#{-1:'请选择包类型',1:'用例包',0:'bug包'}" headerValue="-1" cssStyle="width:100;" cssClass="text_c" onchange="doOnChg(this.value)">
					</ww:select>
			    </td>
			  	 <td class="rightM_center" style="color:red;" align="right">
			  	 执行环境:
			  	 </td>
			    <td class="dataM_left">
					<ww:textfield id="exeEnv" readonly="true" name="dto.testCycleTask.exeEnv"  cssClass="text_c" cssStyle="padding:2 0 0 4;" > </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" style="color:red;">
			  	 计划开始日期:
			  	 </td>
			    <td class="dataM_left">
					<ww:textfield id="planStartDate" name="dto.testCycleTask.planStartDate" readonly="true"  cssClass="text_c" cssStyle="padding:2 0 0 4;" onclick="showCalendar(this);"> </ww:textfield>			    
			    </td>
			  	 <td class="rightM_center" align="right">
			  	 计划结束日期:
			  	 </td>
			    <td  class="dataM_left" >
			       <ww:textfield id="planEndDate" name="dto.testCycleTask.planEndDate" readonly="true"  cssClass="text_c" cssStyle="padding:2 0 0 4;" onclick="showCalendar(this);"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr id="recpiUserIdTr" class="ev_mypm"style="display:none" >
			  	 <td class="rightM_center"  style="color:red;">
			  	 执行人:
			  	 </td>
			    <td class="dataM_left" colspan="3">
			    <ww:hidden id="ownerId" name="dto.testCycleTask.ownerId"></ww:hidden>
					<ww:textfield id="ownerName" name="ownerName"readonly="true"  cssClass="text_c" onfocus="popselWin('ownerId','ownerName');"  cssStyle="width:560;padding:2 0 0 4;"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="ev_mypm">
			  	 <td class="rightM_center" style="color:red;">
			  	 公告内容:
			  	 </td>
			    <td class="dataM_left" colspan="3">
					<textarea name="dto.broMsg.content" id="content" cols="50"
							rows="30" style="width:560;hight:200;padding:2 0 0 4;" Class="text_c">
					</textarea>	
			    </td>
			  </tr>				
			</ww:form>
		    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" width="80" style="border-right:0">
			  	 附件:
			  	 </td>
			    <td class="dataM_left"  colspan="2" width="374" style="border-right:0">
					<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
					<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
					<img src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/img_insert.png" id="insertImg" alt="当前位置插入图片" title="当前位置插入图片" onclick="upLoadAndSub('addUpSub','subChk',1,oEditor)" />
			    </td>
			    <td style="border-right:0">
			    </td>
			  </tr>	
			  <tr class="ev_mypm">
			    <td width="80" align="right">&nbsp;</td>
				<td width="560" style="width:200;padding:2 0 0 4;" colspan="3"><div id="upStatusBar"></div></td>		  
			  </tr>		  
			<tr  class="odd_mypm">
				<td class="dataM_center" align="center" width="640" colspan="4">
					<a class="bluebtn" href="javascript:void(0);"
						onclick="javascript:eraseAttach('eraseAllImg');cW_ch.setModal(false);cW_ch.hide();"
						style="margin-left: 6px"><span> 返回</span> </a>
					<a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="upLoadAndSub('addUpSub','subChk',0,oEditor);"
						style="margin-left: 6px;display:none"><span> 确定</span> </a>
					<a class="bluebtn" href="javascript:void(0);" id="reSetAttaBtn" onclick="$('currUpFile').value='';if(_isIE)$('currUpFile').outerHTML=$('currUpFile').outerHTML" ><span>取消选取的文件</span> </a>
				<td>
		  </tr>	
		  <tr class="ev_mypm">
		    <td colspan="4">&nbsp;</td>
		  </tr>	
		  </ww:form>		  
 		     <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>
	       </table>
	      </div>
	    </div>
	<div id="findDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
	   <div class="objbox" style="overflow:auto;width:100%;">
		<ww:form theme="simple" method="post" id="findForm" name="findForm" namespace="" action="">
		<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
		  <tr class="ev_mypm">
		    <td colspan="4" width="500">&nbsp;
		    </td>
		  </tr>
		  <tr class="odd_mypm">
		  	 <td class="rightM_center" width="125" align="right" >
		  	 标题/内容:
		  	 </td>
		    <td class="dataM_center" width="375" colspan="3"align="left">
				<ww:textfield id="msgTitleF" name="dto.broMsg.title" cssClass="text_c" cssStyle="width:375;padding:2 0 0 4;"> </ww:textfield>			    
		    </td>
		  </tr>	
		  <tr class="ev_mypm">
		  <ww:hidden id="senderIdF" name="dto.broMsg.senderId"></ww:hidden>
		  	 <td class="rightM_center" width="125" align="right" >
		  	 发布人:
		  	 </td>
		    <td class="dataM_center" width="375" colspan="3"align="left">
				<ww:textfield id="senderNameF" name="senderNameF" readonly="true" cssClass="text_c" onfocus="popselWin('senderIdF','senderNameF');" cssStyle="width:375;padding:2 0 0 4;"> </ww:textfield>			    
		    </td>
		  </tr>	
		  <tr class="odd_mypm">
		  	 <td class="rightM_center" width="125" align="right">
		  	 类型:
		  	 </td>
		    <td class="dataM_left" width="475" align="left" colspan="3">
				<ww:select id="msgTypeF" name="dto.broMsg.msgType"
					list="#{-1:'',0:'广播',1:'限定接收人'}" headerValue="-1" cssStyle="width:100;" cssClass="text_c">
				</ww:select>&nbsp;&nbsp;状态:
				<ww:select id="stateF" name="dto.broMsg.state"
					list="#{-1:'',1:'未生效',2:'己生效',3:'己过期'}" headerValue="-1" cssStyle="width:100;" cssClass="text_c">
				</ww:select>
		    </td>
		  </tr>	
		  <tr class="ev_mypm">
		    <td width="500" colspan="4">&nbsp;
		    </td>
		  </tr>
		<tr class="odd_mypm">
			<td class="dataM_center" align="center"  colspan="4">
				<a class="bluebtn" href="javascript:void(0);"
					onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
					style="margin-left: 6px"><span> 返回</span> </a>
				<a class="bluebtn" href="javascript:void(0);" id="queryBtn"onclick="findExe()"
					style="margin-left: 6px;"><span> 确定</span> </a>
			<td>
		</tr>	
		<tr class="ev_mypm">
		    <td width="500" colspan="5">&nbsp;
		    </td>
		</tr>		 			
		</table>	
		</ww:form>
		</div>
	</div>
	<ww:include value="/jsp/common/selCompPerson.jsp"></ww:include>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	</BODY>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/broadcastMsg/broMsgManager.js"></script>
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
   <link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.css">	
   <script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.js"></script>						
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/upload.js");
	</script>
</HTML>
