<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>用户列表</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<BODY bgcolor="#ffffff" style="overflow-y:hidden;overflow-x:hidden;">
		<table cellspacing="0" cellpadding="0" align="center">
			<tr><td><div id="toolbarObj"></div></td></tr>
			<tr><td><div id="gridbox"></div></td></tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
	<script>
	ininPage("toolbarObj", "gridbox", 895);
	var pmBar, importW_ch;
	pageBreakUrl = conextPath+'<pmTag:urlAudit  aduitRwrt="/userManager/userManagerAction!findUsers.action?dto.isAjax=true"/>';
	<pmTag:button page="userManager/user" find="true"/>
	var myId="${session.currentUser.userInfo.id}";
	var opName = "${session.currentUser.userInfo.loginName}";
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,真实姓名,员工编号,登录帐号,联系电话,办公电话,电子信箱,职务,状态,&nbsp;");
    pmGrid.setInitWidths("40,40,140,120,100,120,130,"+nameWidth +",130,65,0");
    pmGrid.setColAlign("center,center,left,left,left,left,left,left,left,center,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str,str,str,str,str,str");
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,false,true,true,true,true,true,true,true,true,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    pageBreakForm = "findForm";
    var currrCount = pmGrid.getRowsNum();
	for(var i = 0; i < currrCount; i++){
		if(pmGrid.cells2(i,10).getValue()=="2"&&pmGrid.cells2(i,4).getValue()!="admin"){
			pmGrid.cells2(i,4).setTextColor("blue");
		}			
	}
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
		if(pmGrid.cells(rowId,4).getValue()=="admin"&&opName=="admin"){
			pmBar.disableItem("setMgrPersion");
		}else if(opName=="admin"){
			pmBar.enableItem("setMgrPersion");
			if(pmGrid.cells(rowId,10).getValue()=="2"){
				pmBar.setItemToolTip("setMgrPersion", "撤销用户管理人员设置使其只可查看本人参与的项目");
			}else{
				pmBar.setItemToolTip("setMgrPersion", "设置用户为管理人员使其可查看任何项目");
			}
		}
	}
  function PasswordStrength(showed){
	  this.showed = (typeof(showed) == "boolean")?showed:true;
	  this.styles = new Array();
	  this.styles[0] = {backgroundColor:"#EBEBEB",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #BEBEBE",borderBottom:"solid 1px #BEBEBE"};
	  this.styles[1] = {backgroundColor:"#FF4545",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #BB2B2B",borderBottom:"solid 1px #BB2B2B"};
	  this.styles[2] = {backgroundColor:"#FFD35E",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #E9AE10",borderBottom:"solid 1px #E9AE10"};
	  this.styles[3] = {backgroundColor:"#95EB81",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #3BBC1B",borderBottom:"solid 1px #3BBC1B"};
	  this.labels= ["弱","中","强"];
	  this.divName = "pwd_div_"+Math.ceil(Math.random()*100000);
	  this.minLen = 5;
	  this.width = "122px";
	  this.height = "16px";
	  this.content = "";
	  this.selectedIndex = 0;
	  this.init();
  }
  PasswordStrength.prototype.init = function(){
	  var s = '<table cellpadding="0" id="'+this.divName+'_table" cellspacing="0" style="width:'+this.width+';height:'+this.height+';">';
	  s += '<tr>';
	  for(var i=0;i<3;i++){
	 		s += '<td id="'+this.divName+'_td_'+i+'" width="33%" align="center"><span style="font-size:1px"> </span><span id="'+this.divName+'_label_'+i+'" style="display:none;font-family: Courier New, Courier, mono;font-size: 12px;color: #000000;">'+this.labels[i]+'</span></td>';
	  }
	  s += '</tr>';
	  s += '</table>';
	  this.content = s;
	  if(this.showed){
	  	document.write(s);
	 		this.copyToStyle(this.selectedIndex);
	  }
  }
  PasswordStrength.prototype.copyToObject = function(o1,o2){
	  for(var i in o1){
	  	o2[i] = o1[i];
	  }
  }
  PasswordStrength.prototype.copyToStyle = function(id){
	  this.selectedIndex = id;
	  for(var i=0;i<3;i++){
		  if(i == id-1){
		  	this.$(this.divName+"_label_"+i).style.display = "inline";
		  }else{
		  	this.$(this.divName+"_label_"+i).style.display = "none";
		  }
	  }
	  for(var i=0;i<id;i++){
	  	this.copyToObject(this.styles[id],this.$(this.divName+"_td_"+i).style);
	  }
	  for(;i<3;i++){
	  	this.copyToObject(this.styles[0],this.$(this.divName+"_td_"+i).style);
	  }
  }
  PasswordStrength.prototype.$ = function(s){
  	return document.getElementById(s);
  }
  PasswordStrength.prototype.setSize = function(w,h){
  	this.width = w;
  	this.height = h;
  }
  PasswordStrength.prototype.setMinLength = function(n){
	  if(isNaN(n)){
	  	return ;
	  }
	  n = Number(n);
	  if(n>1){
	  	this.minLength = n;
	  }
  }
  PasswordStrength.prototype.setStyles = function(){
	  if(arguments.length == 0){
	  	return ;
	  }
	  for(var i=0;i<arguments.length && i < 4;i++){
	  	this.styles[i] = arguments[i];
	  }
	  this.copyToStyle(this.selectedIndex);
  }
  PasswordStrength.prototype.write = function(s){
	  if(this.showed){
	  	return ;
	  }
	  var n = (s == 'string') ? this.$(s) : s;
	  if(typeof(n) != "object"){
	  	return ;
	  }
	  n.innerHTML = this.content;
	  this.copyToStyle(this.selectedIndex);
  }
  PasswordStrength.prototype.update = function(s){
	  if(s.length < this.minLen){
	  	this.copyToStyle(0);
	  	return;
	  }
	  var ls = -1;
	  if (s.match(/[a-z]/ig)){
	  	ls++;
	  }
	  if (s.match(/[0-9]/ig)){
	  	ls++;
	  }
	  if (s.match(/(.[^a-z0-9])/ig)){
	  	ls++;
	  }
	  if (s.length < 6 && ls > 0){
	  	ls--;
	  }
	  switch(ls) {
		  case 0:
		  this.copyToStyle(1);
		  break;
		  case 1:
		  this.copyToStyle(2);
		  break;
		  case 2:
		  this.copyToStyle(3);
		  break;
		  default:
		  this.copyToStyle(0);
	  }
  }
  var loginNameInit ="${dto.user.loginName}";
  
  function importUser(){
  		var filePath = $("importUser").value;
		var window = "\/", linux="\\";
		var index = filePath.lastIndexOf(window);
		if(index<0)
			index=filePath.lastIndexOf(linux);
		var fileName = filePath.substring(index+1, filePath.length);
		var checkFile = new RegExp("^(\\S)+(.)(xls)$");
		if(fileName!=""){
			if(!checkFile.test(fileName)){
				$("importUserText").innerHTML = "       上传文件错误, 请上传excel文件";
			}else
				$("importUserF").submit();
		}else{
			$("importUserText").innerHTML = "        请选择导入用户数据的excel文件";
		}
		importW_ch.setDimension($("importUserDiv").offsetWidth + 5, $("importUserDiv").offsetHeight);
  }
  var ps;
</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
		  <ww:hidden id="oldPwd" name="oldPwd"></ww:hidden>
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="/userManager" action="">
				<ww:hidden id="userId" name="dto.user.id"></ww:hidden>
				<ww:hidden id="isAdmin" name="dto.user.isAdmin"></ww:hidden>
				<ww:hidden id="status" name="dto.user.status"></ww:hidden>
				<ww:hidden id="delFlag" name="dto.user.delFlag"></ww:hidden>
				<ww:hidden id="insertDate" name="dto.user.insertDate"></ww:hidden>
				<ww:hidden id="chgPwdFlg" name="dto.user.chgPwdFlg"></ww:hidden>
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%" id="createTab">
					<tr>
						<td colspan="4"  style="border-right:0;width:340">
							<div id="cUMTxt" align="center" style="color: Blue; padding: 2px">
								&nbsp;
							</div>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="color:red;border-right:0"nowrap>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;登录帐号:
						</td>
						<td colspan="3" class="dataM_left" style="border-right:0">
							<ww:textfield id="loginName" name="dto.user.loginName" cssClass="text_c"  cssStyle="width:340;padding:2 0 0 4;"
								 maxlength="16" onblur="loginNameChk()"></ww:textfield>
						</td>
					</tr>
					<tr id="inVoldPwdR" style="display:none">
						<td align="right" class="rightM_center" style="color:red;border-right:0">
								原密码:
						</td>
						<td colspan="3" class="dataM_left" style="border-right:0;width:340" >
							<ww:password id="voldPwd" name="dto.user.oldPwd" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};"></ww:password>
						</td>
					</tr>
					<tr class="rightM_center" style="color:red;border-right:0">
						<td align="right" class="rightM_center" style="border-right:0">
								密码强度:
						</td>
						<td colspan="3" align="left" style="border-right:0;" >
							 <script language="javascript">
								ps = new PasswordStrength();
								ps.setSize("180","20");
								ps.setMinLength(6);
							 </script>
						</td>										
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="color:red;border-right:0">
								登录密码:
						</td>
						<td style="border-right:0" class="dataM_left" >
							<ww:password id="password" name="dto.user.password" cssClass="text_c" cssStyle="width:120;padding:0 0 0 4;" onblur="chgPwd()"
								onfocus="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};" onkeyup="ps.update(this.value);" maxlength="20"></ww:password>
						</td>
						<td align="right" class="rightM_center" style="color:red;border-right:0" >
								确认密码:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:password id="vpassword" name="vpassword" cssClass="text_c" cssStyle="width:120;padding:0 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};"></ww:password>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="color:red;border-right:0" >
								真实姓名:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="name" name="dto.user.name" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};" maxlength="16"></ww:textfield>
						</td>
						<td align="right"  class="rightM_center" style="color:red;border-right:0" >
								电子信箱:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="email" name="dto.user.email" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;"
								onblur="mailChk()" maxlength="32"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0">
							员工编号:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="employeeId" name="dto.user.employeeId" cssClass="text_c" cssStyle="width:120;;padding:2 0 0 4;" maxlength="16"></ww:textfield>
						</td>
						<td align="right" class="rightM_center" style="border-right:0">
							联系电话:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="tel" name="dto.user.tel" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;" maxlength="20"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0">
							办公电话:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="officeTel" name="dto.user.officeTel" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;" maxlength="20"></ww:textfield>
						</td>
						<td align="right"class="rightM_center"  style="border-right:0">
							职务:
						</td>
						<td style="border-right:0" class="dataM_left">
							<ww:textfield id="headShip" name="dto.user.headShip" cssClass="text_c" cssStyle="width:120;padding:2 0 0 4;" maxlength="20"
								onblur="javascript:if(!isWhitespace(this.value)&&!_isIE){$('cUMTxt').innerHTML='&nbsp;'};"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0">
							所属组:
						</td>
						<td colspan="3" style="border-right:0" class="dataM_left">
							<input type="text" id="groupNames" name="dto.user.groupNames"
								readonly="true" Class="text_c" style="width:340;padding:2 0 0 4;"
								onclick="popMutlGrid('<pmTag:urlAudit  aduitRwrt="../userManager/userManagerAction!choiseGroups.action"/>','groupIds','groupNames',popMutlGridCB());"></input>
						</td>
						<ww:hidden name="dto.user.groupIds" id="groupIds" />
						
					</tr>
					<tr id="questionR"  class="rightM_center" style="display:none">
						<td align="right" class="rightM_center" style="border-right:0">
							找回密码问题:
						</td>
						<td colspan="3" style="border-right:0" class="dataM_left">
							<ww:textfield id="question" name="dto.user.question" cssClass="text_c" cssStyle="width:340;padding:2 0 0 4;" maxlength="20"></ww:textfield>
						</td>
					</tr>
					<tr id="answerR" style="display:none">
						<td  class="rightM_center" style="border-right:0" >
							找回密码答案:
						</td>
						<td colspan="3"class="dataM_left" style="border-right:0">
							<ww:textarea id="answer" name="dto.user.answer" cssClass="text_c" cssStyle="width:340;padding:2 0 0 4;" rows="2"></ww:textarea>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="border-right:0">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="4"  class="dataM_center" align="center" style="border-right:0">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:cuW_ch.setModal(false);cuW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU2_b"onclick="addUpSumbit('<pmTag:urlAudit  aduitRwrt="../userManager/userManagerAction!userMaintence.action"/>','userId','cUMTxt',pmGrid,false,'groupIds',addUpCB());"
								style="margin-left: 6px;display:none"><span> 确定并继续</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="addUpSumbit('<pmTag:urlAudit  aduitRwrt="../userManager/userManagerAction!userMaintence.action"/>','userId','cUMTxt',pmGrid,true,'groupIds',addUpCB());"
								style="margin-left: 6px;"><span> 确定</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('createForm', 'loginName');setUpInfo(upInitInfo);"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/userManager" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm" >
						<td align="right" class="rightM_center">登录帐号:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="loginName_f" name="dto.user.loginName"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" class="rightM_center">真实姓名:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="realName_f" name="dto.user.name"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">员工编号:</td>
						<td style="border-right:0;">
							<ww:textfield id="employeeId_f" name="dto.user.employeeId"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" style="border-right:0;" class="rightM_center">办公电话:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="officeTelno_f" name="dto.user.officeTel"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">
							用户状态:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:select id="userStatus_f" name="dto.user.status" cssClass="text_c"
								list="#{-1:'所有',1:'启用',0:'禁用'}" headerValue="-1"
								cssStyle="width:120;">
							</ww:select>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;">
							所属组:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<select id="groupIds_f" name="dto.user.groupIds" Class="text_c"
								style="width: 120;">
							<option value="-1">所有</option> 
							</select>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="dataM_left" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="findM(pageBreakUrl,'',pmGrid);"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('findForm');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
		<div id="importUserDiv" class="gridbox_light" style="border:0px;display:none;">
			<div class="objbox" style="overflow:auto;width:100%;">
				<ww:form theme="simple" method="post" id="importUserF" name="importUserF" namespace="/userManager" action="userManagerAction!importUsers.action" enctype="multipart/form-data">
					<table id="findTable" class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr class="ev_mypm">
							<td class="leftM_center" colspan="2">&nbsp;</td>
						</tr>
						<tr class="odd_mypm">
							<td class="leftM_center" width="100px;">选择导入的文件:</td>
							<td class="dataM_left"><input name="dto.importUser" id="importUser" type="file" /></td>
						</tr>
						<tr class="ev_mypm">
							<td colspan="2" id="importUserText" style="font-family: Tahoma;font-weight: bold;font-size:12px;color:blue;text-align:left;"></td>
						</tr>
						<tr class="odd_mypm">
							<td colspan="2" align="right" style="padding-right:8px;">
							   <a class="bluebtn" href="javascript:void(0);" 
							   id="dwnTemp"onclick="importUser();"
							   style="margin-left: 6px;"><span> 导入</span> </a>
					       <a class="bluebtn" href="javascript:void(0);"
						onclick="javascript: getTemplate('userTemplet.xls');"
						style="margin-left: 6px;"><span> 下载用户导入模板</span> </a>
							</td>
						</tr>
						<tr class="ev_mypm">
							<td colspan="2" >&nbsp;</td>
						</tr>
					</table>
				</ww:form>
			</div>
		</div>
        <ww:include value="/jsp/common/dialog.jsp"></ww:include>
        <ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	</BODY>
	<script type="text/javascript">
	<pmTag:urlAudit urlVarName ="delUrl"  definedUrlVar="../userManager/userManagerAction!deleteUser.action?dto.user.id="/>
	<pmTag:urlAudit urlVarName ="ldelUrl"  definedUrlVar="../userManager/userManagerAction!ldeleteUser.action?dto.user.id="/>
	<pmTag:urlAudit urlVarName ="upUrl"  definedUrlVar="../userManager/userManagerAction!updateUserInit.action?dto.user.id="/>
	var upInitInfo = "";
	</script>

	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/userManager/userManger.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_botm.js"></script>
	<script type="text/javascript">	
		regBarDefEvent();
	</script>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	<script type="text/javascript">
		var customMessage = "${dto.customMessage}";
		if(customMessage != ""){
			importW_ch = initW_ch(importW_ch,  "importUserDiv", false, 350, 120, "", "导入用户");
			importW_ch.setText("导入用户");
			customMessage = replaceAll(customMessage, "$", "<br>");
			if(customMessage == ""){
				$("importUserText").innerHTML = "          用户导入成功";
			}else if(customMessage == "overCount"){
				$("importUserText").innerHTML = "现有用户加上要导入的用户数己达上限";
			}else{
				$("importUserText").innerHTML = customMessage;
			}
			importW_ch.setDimension($("importUserDiv").offsetWidth + 5, $("importUserDiv").offsetHeight + 10);
		}
	</script>
</HTML>
