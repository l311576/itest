		var repChkCount=0;
		var pwdBk="",reName="0";
		function checkRep(){
			reName="0";
			pwdBk=$("pwdField").value;
			if(isWhitespace($("loginName").value)){
				$("pwdField").value = pwdBk;
				reName="0";
				return;
			}
			if(repChkCount==5){
				$("pwdField").value = pwdBk;
				reName="0";
				return;
			}
			var url = conextPath+"/commonAction!regisChk.action?dto.objName=User&dto.nameVal="+$("loginName").value;
			var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			repChkCount++;
			if(ajaxRest=="true"){
				$("msgD").innerHTML = "所填管理员登录帐号己被注册";
				reName="1";
				return;
			}
			$("msgD").innerHTML="";
			reName="0";
			$("pwdField").value = pwdBk;
		}
		var mailChkCount=0,reMail="0";
		function mailChk(){
			reMail="0";
			if(isWhitespace($("ownereMail").value)){
				$("msgD").innerHTML="";
				reMail="0";
				return;
			}
			if(!isEmailAddress($("ownereMail").value)){
				$("msgD").innerHTML="";
				return;
			}
			if(mailChkCount==5){
				reMail="0";
				return;
			}
			var url = conextPath+"/commonAction!reMailChk.action?dto.objName=User&dto.nameVal="+$("ownereMail").value;
			url+="&dto.namePropName=email";
			var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			mailChkCount++;
			if(ajaxRest=="true"){
				$("msgD").innerHTML = "所填电子信箱己被使用";
				reMail="1";
				return;
			}		
			$("msgD").innerHTML="";
			reMail="0";
		}

	var cuW_ch,tutoriaW_ch;
	function viewGuid(){
		tutoriaW_ch = initW_ch(tutoriaW_ch, "", false, 980, 630,'tutoriaW_ch');
		tutoriaW_ch.attachURL(conextPath+"/help/products/index.html");
		tutoriaW_ch.setText("MYPM产品说明书");	
	    tutoriaW_ch.show();
	    tutoriaW_ch.bringToTop();
	    tutoriaW_ch.setModal(false);
	}
	function registryAcc(){
		if(navigator.userAgent.indexOf("Chrome")>0){
			cuW_ch = initW_ch(cuW_ch, "createDiv", true, 440, 435);
		}else if(_isIE){
			cuW_ch = initW_ch(cuW_ch, "createDiv", true, 440, 410);
		}else if(_isFF){
			cuW_ch = initW_ch(cuW_ch, "createDiv", true, 440, 425);
		}
		cuW_ch.button("close").attachEvent("onClick", function(){
			cuW_ch.setModal(false);
			cuW_ch.hide();
			$("validCodeImage").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
		});
		formReset("createF", "name");
		$("msgD").innerHTML = "";	
		cuW_ch.setText("注册公司(组织)帐户");
		$("validCodeImage2").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
	}
	function adjustLable(typeVal){
		if(typeVal=="1"){
			$("accTypeTd").innerHTML="经营行业:"
			$("accNameTd").innerHTML="公司名称:"
			$("orgSizeTd").innerHTML="公司规模:";
			$("accIntroTd").innerHTML="公司简介:";
			$("accAddressTd").innerHTML="公司地址:";
			$("accTelpTd").innerHTML="公司电话:";
			$("accFaxTd").innerHTML="公司传真:";
			$("accWebTd").innerHTML="公司网址:";			
				return;
		}
		$("accTypeTd").innerHTML="所在行业:"
		$("accNameTd").innerHTML="组织名称:"
		$("orgSizeTd").innerHTML="组织规模:";
		$("accIntroTd").innerHTML="组织简介:";
		$("accAddressTd").innerHTML="组织地址:";
		$("accTelpTd").innerHTML="组织电话:";
		$("accFaxTd").innerHTML="组织传真:";
		$("accWebTd").innerHTML="组织网址:";
	}
	function checkAndSub(){
		if(isWhitespace($("name").value)){
			$("msgD").innerHTML = "请填写"+$("accNameTd").innerHTML;
			$("name").focus();
			return;
		}else if($("companyType").value==0){
			$("msgD").innerHTML = "请选择"+$("accTypeTd").innerHTML;
			$("companyType").focus();
			return;		
		}else if($("companySize").value==0){
			$("msgD").innerHTML = "请选择"+$("orgSizeTd").innerHTML;
			$("companySize").focus();
			return;		
		}else if(isWhitespace($("remark").value)){
			$("msgD").innerHTML = "请填写"+$("accIntroTd").innerHTML;
			$("remark").focus();
			return;	
		}else if(isWhitespace($("address").value)){
			$("msgD").innerHTML = "请填写"+$("accAddressTd").innerHTML;
			$("address").focus();
			return;	
		}else if(isWhitespace($("telephone").value)){
			$("msgD").innerHTML = "请填写"+$("accTelpTd").innerHTML;
			$("telephone").focus();
			return;	
		}else if(isWhitespace($("owner").value)){
			$("msgD").innerHTML = "请填写管理员姓名";
			$("owner").focus();
			return;		
		}else if(isWhitespace($("loginName").value)){
			$("msgD").innerHTML = "请填写管理员登录帐号";
			$("loginName").focus();
			return;	
		}else if(isWhitespace($("pwdField").value)){
			$("msgD").innerHTML = "请填写管理员登录密码";
			$("pwdField").focus();
			return;
		}else if(!checkIsOverLong($("pwdField").value,5)){
			$("msgD").innerHTML = "密码不能少于6位";
			$("pwdField").focus();
			return false;
		}else if($("vpassword").value!=$("pwdField").value){
			$("msgD").innerHTML = "确认密码不匹配";
			$("vpassword").focus();
			return;		
		}else if(isWhitespace($("ownereMail").value)){
			$("msgD").innerHTML = "请填写管理员邮箱";
			$("ownereMail").focus();
			return;
		}else if(!isEmailAddress($("ownereMail").value)){
			$("msgD").innerHTML = "管理员邮箱格式不正确";
			$("ownereMail").focus();
			return;		
		}else if(checkIsOverLong($("telephone").value,20)){
			$("msgD").innerHTML = $("accTelpTd").innerHTML+"不能超过20位";
			$("telephone").focus();
			return;		
		}else if(checkIsOverLong($("fax").value,20)){
			$("msgD").innerHTML = $("accFaxTd").innerHTML+"不能超过20位";
			$("fax").focus();
			return;			
		}else if(checkIsOverLong($("ownerTel").value,20)){
			$("msgD").innerHTML = "管理员电话不能超过20位";
			$("ownerTel").focus();
			return;			
		}else if(checkIsOverLong($("ownereMail").value,32)){
			$("msgD").innerHTML = "管理员邮箱不能超过32位";
			$("ownereMail").focus();
			return;				
		}else if(checkIsOverLong($("question").value,20)){
			$("msgD").innerHTML = "密码问题不能超过20位";
			$("question").focus();
			return;			
		}else if(checkIsOverLong($("answer").value,50)){
			$("msgD").innerHTML = "密码答案不能超过50位";
			$("answer").focus();
			return;			
		}else if($("knowWay1").checked==false&&$("knowWay2").checked==false&&$("knowWay3").checked==false){
			$("msgD").innerHTML = "请选择了解Mypm渠道";
			return;			
		}else if($("agree").checked==false){
			$("msgD").innerHTML = "不接受使用协义及隐私政策，不允许注册";
			return;
		}else if(isWhitespace($("viewCode2").value)){
			$("msgD").innerHTML = "请填写验证码";
			$("viewCode2").focus();
			return;
		}else if(reMail=="1"){
			$("msgD").innerHTML = "所填电子信箱己被使用";
			return;
		}else if(reName=="1"){
			$("msgD").innerHTML = "所填管理员登录帐号己被注册";
			return;
		}else if(!speCharChk()){
			$("msgD").innerHTML = "不能输入特殊字符";
			return;		
		}
		$("msgD").innerHTML = "";
		var ajaxRest = dhtmlxAjax.postSync(conextPath+"/userManager/userManagerAction!regisAct.action", "createF").xmlDoc.responseText;
		if(ajaxRest=="success"){
			$("loginlName").value = $("loginName").value;
			$("loginPwd").value = $("pwdField").value;
			$("viewCode").value = $("viewCode2").value;
			$("isCheckViewCode").value="1";
			loginCheck();
			cuW_ch.setModal(false);
			cuW_ch.hide();
			return;
		}else if(ajaxRest=="reName"){
			$("msgD").innerHTML = "所填管理员登录帐号己被抢注";
			reName="1";
			$("validCodeImage2").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
		}else if(ajaxRest=="reMail"){
			$("msgD").innerHTML = "所填管理员登邮箱己被注册";
			reMail="1";
			$("validCodeImage2").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
		}else if(ajaxRest=="viewCdError"){
			$("msgD").innerHTML = "所填验证码不正确";
		}else if(result=="overdue"){
			$("msgD").innerHTML = "会话过期请重填验证码";
			$("validCodeImage2").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
		}else{
			$("msgD").innerHTML =  "注册发生错误请稍后再试";
			$("validCodeImage2").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
		}
		return;
	}
    function checkHasChi(str){
        re = /[\u4E00-\u9FA0]/;//汉字
        if (re.test(str)){
            return 1;
        } else{
            return 0;
        }
    }
    function checkIsOverLong(str,longtest){
        var len=0;
        for(l=0;l<str.length;l++){
            if(checkHasChi(str.charAt(l))==1){
                len+=2;
            } else{
                len+=1;
            }
            if(len>parseInt(longtest)){
                return true;
            }
        }
        return false;
    }
	function isEmailAddress(value, allowEmpty){
	  var strText = new String(value);
	  if (allowEmpty == true && value.length == 0) {
	    return true;
	  }
	  var strPattern = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  var strPatt = /[^a-zA-Z0-9-@\._]/;
	  if (strText.match(strPatt)) {
	     return false;
	  }
	  aryResult = strText.match(strPattern);
	  if(aryResult == null) {
	  return false;
	  } else {
	  return true;
	 }
	}
  function PasswordStrength(showed){
	  this.showed = (typeof(showed) == "boolean")?showed:true;
	  this.styles = new Array();
	  this.styles[0] = {backgroundColor:"#EBEBEB",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #BEBEBE",borderBottom:"solid 1px #BEBEBE"};
	  this.styles[1] = {backgroundColor:"#FF4545",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #BB2B2B",borderBottom:"solid 1px #BB2B2B"};
	  this.styles[2] = {backgroundColor:"#FFD35E",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #E9AE10",borderBottom:"solid 1px #E9AE10"};
	  this.styles[3] = {backgroundColor:"#95EB81",borderLeft:"solid 1px #FFFFFF",borderRight:"solid 1px #3BBC1B",borderBottom:"solid 1px #3BBC1B"};
	  
	  this.labels= ["弱","中","强"];
	  
	  this.divName = "pwd_div_"+Math.ceil(Math.random()*100000);
	  this.minLen = 5;
	  
	  this.width = "122px";
	  this.height = "16px";
	  
	  this.content = "";
	  
	  this.selectedIndex = 0;
	  
	  this.init();
  }
  PasswordStrength.prototype.init = function(){
	  var s = '<table cellpadding="0" id="'+this.divName+'_table" cellspacing="0" style="width:'+this.width+';height:'+this.height+';">';
	  s += '<tr>';
	  for(var i=0;i<3;i++){
	 		s += '<td id="'+this.divName+'_td_'+i+'" width="33%" align="center"><span style="font-size:1px"> </span><span id="'+this.divName+'_label_'+i+'" style="display:none;font-family: Courier New, Courier, mono;font-size: 12px;color: #000000;">'+this.labels[i]+'</span></td>';
	  }
	  s += '</tr>';
	  s += '</table>';
	  this.content = s;
	  if(this.showed){
	  	document.write(s);
	 		this.copyToStyle(this.selectedIndex);
	  }
  }
  PasswordStrength.prototype.copyToObject = function(o1,o2){
	  for(var i in o1){
	  	o2[i] = o1[i];
	  }
  }
  PasswordStrength.prototype.copyToStyle = function(id){
	  this.selectedIndex = id;
	  for(var i=0;i<3;i++){
		  if(i == id-1){
		  	this.$(this.divName+"_label_"+i).style.display = "inline";
		  }else{
		  	this.$(this.divName+"_label_"+i).style.display = "none";
		  }
	  }
	  for(var i=0;i<id;i++){
	  	this.copyToObject(this.styles[id],this.$(this.divName+"_td_"+i).style);
	  }
	  for(;i<3;i++){
	  	this.copyToObject(this.styles[0],this.$(this.divName+"_td_"+i).style);
	  }
  }
  PasswordStrength.prototype.$ = function(s){
  	return document.getElementById(s);
  }
  PasswordStrength.prototype.setSize = function(w,h){
  	this.width = w;
  	this.height = h;
  }
  PasswordStrength.prototype.setMinLength = function(n){
	  if(isNaN(n)){
	  	return ;
	  }
	  n = Number(n);
	  if(n>1){
	  	this.minLength = n;
	  }
  }
  PasswordStrength.prototype.setStyles = function(){
	  if(arguments.length == 0){
	  	return ;
	  }
	  for(var i=0;i<arguments.length && i < 4;i++){
	  	this.styles[i] = arguments[i];
	  }
	  this.copyToStyle(this.selectedIndex);
  }
  PasswordStrength.prototype.write = function(s){
	  if(this.showed){
	  	return ;
	  }
	  var n = (s == 'string') ? this.$(s) : s;
	  if(typeof(n) != "object"){
	  	return ;
	  }
	  n.innerHTML = this.content;
	  this.copyToStyle(this.selectedIndex);
  }
  PasswordStrength.prototype.update = function(s){
	  if(s.length < this.minLen){
	  	this.copyToStyle(0);
	  	return;
	  }
	  var ls = -1;
	  if (s.match(/[a-z]/ig)){
	  	ls++;
	  }
	  if (s.match(/[0-9]/ig)){
	  	ls++;
	  }
	  if (s.match(/(.[^a-z0-9])/ig)){
	  	ls++;
	  }
	  if (s.length < 6 && ls > 0){
	  	ls--;
	  }
	  switch(ls) {
		  case 0:
		  this.copyToStyle(1);
		  break;
		  case 1:
		  this.copyToStyle(2);
		  break;
		  case 2:
		  this.copyToStyle(3);
		  break;
		  default:
		  this.copyToStyle(0);
	  }
  }
  function speCharChk(){
	var form = document.getElementById("createF");
    var elements = form.elements;  
    for (i = 0; i < elements.length; ++i) {
      var element = elements[i];
      if(element.type == "text" || element.type == "textarea" || element.type == "hidden"){
      	if(includeSpeChar(element.value)){
      		return false;
      	}
      }
     }
    return true;
  }
  function includeSpeChar(StrVal){
  	var speStr="~ ` ! # $ % ^ & * ( ) [ ] { } ; ' : \" , ， < >";
  	var speStrArr = speStr.split(" ");
  	if(typeof(StrVal)=="undefined"||isWhitespace(StrVal))
  		return false;
  	for(var i=0; i<speStrArr.length; i++){
  		if(StrVal.indexOf(speStrArr[i])>=0){
  			return true;
  		}
  	}
  	return false;
  }