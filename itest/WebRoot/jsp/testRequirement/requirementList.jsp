﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>

<html>
	<head>
	
		<title>测试需求分解</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<script type="text/javascript">
	</script>
	<body bgcolor="#ffffff" >
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top"><div id="toolbarObj"></div></td>
			</tr>
			<tr>
				<td valign="top"><div id="gridbox"></div></td>
			</tr>
		</table>
		<ww:hidden id="taskId2" name="dto.taskId"></ww:hidden>
		<script type="text/javascript">
		//var taskOpts = Array(Array('00000000000000000000000000000000', 'obj', '请选择任务'));
		ininPage("toolbarObj", "gridbox", 240);
		<pmTag:button page="outLineManager"  checkAll="false" pagination="false"/>
		try{pmBar.disableItem("subMod");}catch(err){}
		</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="/outLineManager" action="">
				<ww:hidden id="taskId" name="dto.taskId"></ww:hidden>
				<ww:hidden id="currNodeId" name="dto.currNodeId"></ww:hidden>
				<ww:hidden id="command" name="dto.command" ></ww:hidden>
				<ww:hidden id="currLevel" name="dto.currLevel" ></ww:hidden>
				<ww:hidden id="parentNodeId" name="dto.parentNodeId" ></ww:hidden>
				<ww:hidden id="moduleState" name="dto.moduleState" ></ww:hidden>
				<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr id="cUMTxtTR" class="ev_mypm">
						<td colspan="4" class="tdtxt" align="center">
							<div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" Class="text_c" align="center">
							<input type="text" name="dto.moduleData[0]" id="module1"
								style="width: 200; padding: 2 0 0 4;" class="text_c" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[1]" id="module2"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[2]" id="module3"
								style="width: 200; padding: 2 0 0 4;" class="text_c" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[3]" id="module4"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[4]" id="module5"
								style="width: 200; padding: 2 0 0 4;" class="text_c" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[5]" id="module6"
								style="width: 200; padding: 2 0 0 4;" class="text_c" />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[6]" id="module7"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[7]" id="module8"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[8]" id="module9"
								style="width: 200; padding: 2 0 0 4;" class="text_c" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center">
							<input type="text" name="dto.moduleData[9]" id="module10"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4"  align="center">
						<input type="radio" name="addchildRd" id="addchildRd" checked="checked" value="addchild" onclick="javascript:$('command').value='addchild';;$('addBroRd').checked=false;"/><label for="addchild"> <font color="blue">子需求</font></label>
						<input type="radio" name="addBroRd" id="addBroRd"  value="addBro" onclick="javascript:$('command').value='addBro';$('addchildRd').checked=false;"/><label for="addBroRd"><font color="blue">同级需求</font></label>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center">

							<a class="bluebtn" href="javascript:void(0);" id="save_b2"onclick="javascript:cuW_ch.setModal(false);cuW_ch.hide();"
					        style="margin-left: 6px;"><span>返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="save_b2"onclick="addNode2db();"
					        style="margin-left: 6px;"><span>确定并继续</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="save_b"onclick="addNode2db('closeFlg');"
					        style="margin-left: 6px;"><span> 确定</span> </a>
						</td>
					</tr>
				</table>
			</ww:form>
		 </div>
		</div>
		<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/outlineManager/outLineBase.js"></script>	
	<div id="selPeopleDiv"   class="cycleTask gridbox_light" style="border:0px;display:none;">
	  <div class="objbox" style="overflow:auto;width:100%;">
		  <ww:form theme="simple" method="post" id="assignForm" name="assignForm" namespace="/outLineManager" action="">
		    <ww:hidden id="assignNIds" name="dto.assignNIds"></ww:hidden>
		    <ww:hidden id="userIds" name="dto.userIds"></ww:hidden>
		    <ww:hidden id="parentIdes" name="dto.parentIdes"></ww:hidden>
			<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr>
				  <td align="center" colspan="2" style="border-right:0">
				  	   <a class="bluebtn" href="javascript:void(0);" id="selPeople_b"onclick="selPeople();"
					        style="margin-left: 6px;"><span> 确定</span> </a>
				  	  <a class="bluebtn" href="javascript:void(0);" id="fres_btn"onclick="initSleEdPeople(getChecked(),'fresh');"
					        style="margin-left: 6px;"><span> 刷新备选人员</span> </a>
				     
				  </td>
				</tr>
				<tr height="5">
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr height="290">
			    	<td align="center" valign="top" width="160" style="border-right:0">
					  <div id="selGridbox" ></div>
					</td>
			    	<td align="center" valign="top" width="160" style="border-right:0">
					  <div id="seledGridbox"></div>
					</td>
				</tr>
			</table>
		</ww:form>
	  </div>
  </div>
  <ww:include value="/jsp/common/dialog.jsp"></ww:include>	
	</body>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
</html>
