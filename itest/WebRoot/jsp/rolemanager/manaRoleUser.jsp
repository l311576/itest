<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>用户列表</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<BODY bgcolor="#ffffff">
	<script type="text/javascript">
	function styleChange(num){
	 	 	switch(num){
				case 1:
					$("userInRoleTab").className="jihuo";
					$("userOutRoleTab").className="nojihuo";
					loadUserInrRole();
					break;
				case 2:
					$("userInRoleTab").className="nojihuo";
					$("userOutRoleTab").className="jihuo";
					 loadUserOutrRole();
					break;
	 		}
	 } 
	 function loadUserInrRole(){
	 	$("userInRoleDiv").style.display="";
	 	$("userOutRoleDiv").style.display="none";
	 }
	 var outLoad = false;
	 var pageNo2=0,pageSize2=10,pageCount2=0,inPageNo,inPageSize,intPageCount,pmBar2,pmGrid2;
	 function loadUserOutrRole(){
	 	$("toolbarObj2").style.width=$("toolbarObj").style.width;
	 	$("gridbox2").style.width=$("gridbox").style.width;
		pmGrid2._setColumnSizeR(0, 40);
		pmGrid2._setColumnSizeR(1, 40);
		pmGrid2._setColumnSizeR(2,pmGrid.getColWidth(2));
		pmGrid2._setColumnSizeR(3, 120);
		pmGrid2._setColumnSizeR(4, 100);
		pmGrid2._setColumnSizeR(5, 120);
		pmGrid2._setColumnSizeR(6, 130);
		pmGrid2._setColumnSizeR(7, 130);
		pmGrid2._setColumnSizeR(8, 65);
	 	if(outLoad){
			$("userOutRoleDiv").style.display="";	 
	 		$("userInRoleDiv").style.display="none";	
	 		return;	 	
	 	}
		var url = conextPath+"/role/roleAction!selingUerList.action?dto.role.roleId="+parent.pmGrid.getSelectedId();
		var ajaxRest = postSub(url,"");	 
		if(ajaxRest=="failed"){
			hintMsg('加载帐户失帐');
			return;
		}
		if(ajaxRest.split("$")[1]!="")
		outLoad=true;
		$("outListStr").value=ajaxRest;
		$("userOutRoleDiv").style.display="";	 
	 	$("userInRoleDiv").style.display="none";
	    pageInfoBk();
		initGrid(pmGrid2,"outListStr",pmBar2);
		pageInfoRce();	
	}
	</script>		
		<div  id="tabSw">
			<table border="0" style="width: 160" cellpadding="0" cellspacing="0" id="tabSwTable">
				<tr>
					<td class="jihuo" id="userInRoleTab" width="80" 
						align="right">
						<strong><a onclick="styleChange(1)"
							href="javascript:void(0);">角色内帐户</a> </strong>
					</td>
					<td class="nojihuo" id="userOutRoleTab" width="80"
						align="left">
						<strong><a onclick="styleChange(2)"
							href="javascript:void(0);">角色外帐户</a> </strong>
					</td>
				</tr>
			</table>
		</div>		
		<div id="userInRoleDiv">
			<table width="880" cellspacing="0" cellpadding="0">
				<tr>
					<td style="width:100%" colspan='9'>
						<div id="toolbarObj"></div>
					</td>
				</tr>
				<tr>
					<td style="width:100%" colspan='9'>
						<div id="gridbox"></div>
					</td>
				</tr>
			</table>
		</div>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
		<input type="hidden" id="outListStr" name="outListStr" value=""/>
		<div id="userOutRoleDiv"  style="display:none">
			<table width="880" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" style="width:100%" colspan='9'>
						<div id="toolbarObj2"></div>
					</td>
				</tr>
				<tr>
					<td align="left" style="width:100%" colspan='9'>
						<div id="gridbox2"></div>
					</td>
				</tr>
			</table>		
		</div>
	<script>
	pageBreakUrl = conextPath+'/userManager/userManagerAction!findUsers.action?dto.isAjax=true';
	var  pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("delFrom",1 ,'从角色删除', "");
	pmBar.addSeparator("broAuSp",2);
	pmBar.addButton("find",3 , "", "search.gif");
	pmBar.setItemToolTip("find", "查询");
	pmBar.addButton("reFreshP",4 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addSeparator("userMaSp",5);
	pmBar.addButton("retMain",6 ,'返回', "");
	pmBar.addSeparator("broAuSp",7);
	pmBar.addButton("first",8 , "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",9, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",10, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",11, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",12, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",13, "", 25);
	pmBar.addText("pageMessage",14, "");
	pmBar.addText("pageSizeText",15, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
	pmBar.addButtonSelect("pageP",16, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");
	pmBar.addText("pageSizeTextEnd",17, "条");
	pmBar.setListOptionSelected('pageP','id1');
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,姓名,员工编号,登录帐号,联系电话,办公电话,职务,状态");
    if(_isIE){
    	 pmGrid.setInitWidths("20,40,133,120,100,120,130,130,65");
    }else{
    	 pmGrid.setInitWidths("20,40,133,120,100,122,130,130,65");
    }
    pmGrid.setColAlign("center,center,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str,str,str,str");
	pmGrid.enableAutoHeight(true, 500);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,false,true,true,true,true,true,true,true");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    pmGrid._setColumnSizeR(0, 40);
	pmBar.attachEvent("onClick", function(id) {
		if(id =="retMain"){
			parent.useMaW_ch.setModal(false);
			parent.useMaW_ch.hide();
		}
	});
	$("toolbarObj2").style.width=$("toolbarObj").style.width;
 	$("gridbox2").style.width=$("gridbox").style.width;
 	pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmBar2 = new dhtmlXToolbarObject("toolbarObj2");
	pmBar2.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar2.addButton("addTo",1 ,'增加到角色', "");
	pmBar2.addSeparator("broAuSp",2);
	pmBar2.addButton("find",3 , "", "search.gif");
	pmBar2.setItemToolTip("find", "查询");
	pmBar2.addButton("reFreshP",4 , "", "page_refresh.gif");
	pmBar2.setItemToolTip("reFreshP", "刷新页面");
	pmBar2.addSeparator("userMaSp",5);
	pmBar2.addButton("retMain",6 ,'返回', "");
	pmBar2.addSeparator("broAuSp",7);
	pmBar2.addButton("first",8 , "", "first.gif", "first.gif");
	pmBar2.setItemToolTip("first", "第一页");
	pmBar2.addButton("pervious",9, "", "pervious.gif", "pervious.gif");
	pmBar2.setItemToolTip("pervious", "上一页");
	pmBar2.addSlider("slider",10, 80, 1, 30, 1, "", "", "%v");
	pmBar2.setItemToolTip("slider", "滚动条翻页");
	pmBar2.addButton("next",11, "", "next.gif", "next.gif");
	pmBar2.setItemToolTip("next", "下一页");
	pmBar2.addButton("last",12, "", "last.gif", "last.gif");
	pmBar2.setItemToolTip("last", "末页");
	pmBar2.addInput("page",13, "", 25);
	pmBar2.addText("pageMessage",14, "");
	pmBar2.addText("pageSizeText",15, "每页");
	pmBar2.addButtonSelect("pageP",16, "page", opts);
	pmBar2.setItemToolTip("pageP", "每页记录数");
	pmBar2.addText("pageSizeTextEnd",17, "条");
	pmBar2.setListOptionSelected('pageP','id1');
	pmGrid2 = new dhtmlXGridObject('gridbox2');
	pmGrid2.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
   	pmGrid2.setHeader("&nbsp;,序号,姓名,员工编号,登录帐号,联系电话,办公电话,职务,状态");
   	if(_isIE){
    	 pmGrid2.setInitWidths("20,40,133,120,100,120,130,130,65");
    }else{
    	 pmGrid2.setInitWidths("20,40,133,120,100,122,130,130,65");
    }
    pmGrid2.setColAlign("center,center,left,left,left,left,left,left,left");
    pmGrid2.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid2.setColSorting("int,int,str,str,str,str,str,str,str");
	pmGrid2.enableAutoHeight(true, 500);
    pmGrid2.init();
    pmGrid2.enableRowsHover(true, "red");
    pmGrid2.enableTooltips("false,false,true,true,true,true,true,true,true");
    pmGrid2.setSkin("light");
    initGrid(pmGrid2,"outListStr",pmBar2);
    pmGrid2.attachEvent("onCheckbox",doOnCheck);
	pmBar2.attachEvent("onClick", function(id) {
		if(id =="retMain"){
			parent.useMaW_ch.setModal(false);
			parent.useMaW_ch.hide();
		}
	});	
	pmGrid2._setColumnSizeR(0, 40);
	pmGrid2._setColumnSizeR(1, 40);
	pmGrid2._setColumnSizeR(2,pmGrid.getColWidth(2));
	pmGrid2._setColumnSizeR(3, 120);
	pmGrid2._setColumnSizeR(4, 100);
	pmGrid2._setColumnSizeR(5, 120);
	pmGrid2._setColumnSizeR(6, 130);
	pmGrid2._setColumnSizeR(7, 130);
	pmGrid2._setColumnSizeR(8, 65);
	
	function chkAppend(rowId,state){
		if($("userOutRoleDiv").style.display==""){
			if(selUserIds!=""){
				if(state){
					selUserIds+=" "+rowId;
					appendSelData(rowId);
				}else{
					delSelData(rowId);
					var idTmp = selUserIds.split(rowId);
					selUserIds =idTmp[0].replace(/\s+$|^\s+/g,"")+" "+idTmp[1].replace(/\s+$|^\s+/g,"");
				}			
			}else if(selUserIds==""&&state){
				selUserIds=rowId;
				appendSelData(rowId);
			}			
		}else{
			if(delUserIds!=""){
				if(state){
					delUserIds+=" "+rowId;
					appendSelData(rowId);
				}else{
					var idTmp = delUserIds.split(rowId);
					delUserIds =idTmp[0].replace(/\s+$|^\s+/g,"")+" "+idTmp[1].replace(/\s+$|^\s+/g,"");
					delSelData(rowId);
				}			
			}else if(delUserIds==""&&state){
				delUserIds=rowId;
				appendSelData(rowId);
			}			
		}
	}
    function doOnCheck(rowId,cellInd,state){
    	if($("userOutRoleDiv").style.display==""){
			pmGrid2.setSelectedRow(rowId);
			 chkAppend(rowId,state);
		}else{
			pmGrid.setSelectedRow(rowId);
			chkAppend(rowId,state);
		}
		return true;
	}
</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/rolemanager/user.js"></script>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:460">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/userManager" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm" >
						<td align="right" class="rightM_center">登录帐号:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="loginName_f" name="dto.user.loginName"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" class="rightM_center">真实姓名:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="realName_f" name="dto.user.name"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">员工编号:</td>
						<td style="border-right:0;">
							<ww:textfield id="employeeId_f" name="dto.user.employeeId"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" style="border-right:0;" class="rightM_center">办公电话:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="officeTelno_f" name="dto.user.officeTel"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">
							用户状态:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:select id="userStatus_f" name="dto.user.status" cssClass="text_c"
								list="#{-1:'所有',1:'活动',0:'禁用'}" headerValue="-1"
								cssStyle="width:120;">
							</ww:select>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;">
							所属组:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<select id="groupIds_f" name="dto.user.groupIds" Class="text_c"
								style="width: 120;">
							<option value="-1">所有</option> 
							</select>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="dataM_left" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="efind();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('findForm');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
		<script type="text/javascript">

		</script>
        <ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
</HTML>
