	var selUserIds = ""; 
	var delUserIds = "";
	var selUserData = new Object();
	var delUserData = new Object();
	function doOnSelect(rowId,index){
		if($("userOutRoleDiv").style.display=="")
			pmGrid2.cells(rowId, 0).setValue(true);
		else
			pmGrid.cells(rowId, 0).setValue(true);
	} 
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
	});
	pmBar2.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize2);
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
	});
	pmBar2.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar2.setValue("page", pageNo2);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount2){
			pageNoTemp = pageCount2;
		}
		pageAction(pageNoTemp, pageSize2);
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
			pageAction(pageNo, pageSize);
		}else if(id == "find"){
			find("findDiv");
		}else if(id == "delFrom"){
			if(delUserIds==""){
				hintMsg("请选择要从角色中删除的帐户");
				return;
			}
			cfDialog("deleteExe","您确定要把选择的帐户从到角色中删除",false);
		}else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	pmBar2.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
			pageAction(pageNo2, pageSize2);
		}else if(id == "find"){
			find("findDiv");
		}else if(id == "addTo"){
			if(selUserData==""){
				hintMsg("请选择要增加到角色中的帐户");
				return;
			}		
			cfDialog("addToRole","您确定要把选择的帐户添加到角色中",false);
		}else if(id == "first"){
			pageAction(1, pageSize2);
		}else if(id == "last"){
			pageAction(pageCount2, pageSize2);
		}else if(id == "next"){
			pageAction(pageNo2 + 1, pageSize2);
		}else if(id == "pervious"){
			pageAction(pageNo2 -1, pageSize2);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar2.getListOptionText("pageP", id));
			if(pageSize2==pageSizeTemp){
				return;
			}
			pageSize2 = pageSizeTemp;
			pageAction(pageNo2, pageSize2);
		}
	});
	function pageAction(pageNov, pageSizev){
		var gridObj = pmGrid;
		var braObj = pmBar;
		var url = conextPath+"/role/roleAction!roleUserList.action";
		if($("userOutRoleDiv").style.display==""){
			url = conextPath+"/role/roleAction!selingUerList.action";
			gridObj = pmGrid2;
			braObj = pmBar2;
		}
		url+="?dto.role.roleId="+parent.pmGrid.getSelectedId()+"&dto.pageSize=" + pageSizev+"&dto.pageNo="+ pageNov;
		if($("userOutRoleDiv").style.display==""&&pageNov>pageCount2){
			pmBar2.setValue("page", pageNov);
			return ;
		}else if(pageNov>pageCount){
			pmBar.setValue("page", pageNov);
		}
		var ajaxRest = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText;
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		gridObj.clearAll();
	   		gridObj.parse(eval("(" + userJson[1] +")"), "json");
	    	pageInfoBk();
    		setPageNoSizeCount(userJson[0],braObj);
    		pageInfoRce();
    		if($("userOutRoleDiv").style.display==""){
    			setRowNum(gridObj,"","",pageNo2,pageSize2);
    			pageArrowSet();
    		}else
	   			setRowNum(gridObj);
	   		markerChk();
   		}
	}

	function addToRole(){
		if(selUserIds==""){
			hintMsg("请选择帐户");
			return;
		}
		var url = conextPath+"/role/roleAction!addUserToRole.action?dto.role.roleId="+parent.pmGrid.getSelectedId()+"&dto.userIds="+selUserIds;
		var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		if(ajaxRest=="failed"){
			hintMsg("保存数据时发生错误");
			return;
		}else if(ajaxRest=="success"){
			var idArr = selUserIds.split(" ");
			var rowData = "";
			for(var i=0; i<idArr.length; i++){
				rowData = "0,,"+getRowData(idArr[i]);
				delSelData(idArr[i]);
				pmGrid.addRow(idArr[i],rowData,0);
				if(pmGrid2.getRowIndex(idArr[i])>=0){
					adjustMainGrid("add",rowData.split(",")[2]);
					pmGrid2.deleteRow(idArr[i]);
				}
			}
			setRowNum(pmGrid);
			pmGrid.setSizes();
			selUserIds="";
			clsoseCfWin();
		}
	} 
	function deleteExe(){
		var url = conextPath+"/role/roleAction!delUsrFromRole.action?dto.role.roleId=";
		url += parent.pmGrid.getSelectedId()+"&dto.userIds="+delUserIds;
		var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		if(ajaxRest=="success"){
			var idArr = delUserIds.split(" ");
			var rowData = "";
			for(var i=0; i<idArr.length; i++){
				rowData = "0,,"+getRowData(idArr[i]);
				delSelData(idArr[i]);
				pmGrid2.addRow(idArr[i],rowData,0);
				if(pmGrid.getRowIndex(idArr[i])>=0){
					adjustMainGrid("del",rowData.split(",")[2]);
					pmGrid.deleteRow(idArr[i]);
				}
			}
			setRowNum(pmGrid2);
			pmGrid2.setSizes();
			delUserIds="";
			clsoseCfWin();
			return;
		}
		hintMsg("删除时发生错误");
	}
	function adjustMainGrid(opFlg,userName){
		var rowId = parent.pmGrid.getSelectedId();
		var oldValue = parent.pmGrid.cells(rowId,3).getValue();
		if(opFlg=="add"){
			if(oldValue=="")
				parent.pmGrid.cells(rowId,3).setValue(userName);
			else
			   parent.pmGrid.cells(rowId,3).setValue(oldValue+" "+userName);
		}else{
			if(oldValue.indexOf(" ")>0){
				var nameArr = oldValue.split(userName);
				parent.pmGrid.cells(rowId,3).setValue(nameArr[0]+" "+nameArr[1]);
			}else{
				parent.pmGrid.cells(rowId,3).setValue("");
			}
		}
	}
	
	function getRowData(rowId){
		if($("userOutRoleDiv").style.display=="")
			return selUserData[rowId];
		else
		   return delUserData[rowId];
	}
	function appendSelData(rowId){
		var rowDatas ="";
		var gridObj=pmGrid2;
		var dataRec = selUserData;
		if($("userOutRoleDiv").style.display=="none"){
			dataRec= delUserData;
			gridObj = pmGrid;
		}
		for(var i=2; i<9; i++){
			var val = gridObj.cells(rowId,i).getValue();
			if(i<(gridObj.getColumnsNum()-1))
				rowDatas+=val+",";
			else
			    rowDatas+=val;
		}
		dataRec[rowId]=rowDatas;
	}
	function delSelData(rowId){
		var dataRec = selUserData;
		if($("userOutRoleDiv").style.display=="none"){
			dataRec= delUserData;
		}
		dataRec[rowId]="";
	}

	function find(divId){
		$("tabSwTable").style.border="0";
		if(typeof fW_ch=="undefined"){
			findCB();
		}
		fW_ch = initW_ch(fW_ch, divId, false, 460, 150);
		fW_ch.setText("查询");
		$("loginName_f").focus();
	}	
	function findCB(){
		var groupUrl = conextPath+"/userManager/userManagerAction!groupSel.action";
		var groupSel  = dhtmlxAjax.postSync(groupUrl, "").xmlDoc.responseText;
		$("groupIds_f").options.length = 1;
			if(groupSel != ""){
				var options = groupSel.split("^");
				for(var i = 0; i < options.length; i++){
					if(options[i].split(";")[0] != "")
						var selvalue = options[i].split(";")[0] ;
						var selable = options[i].split(";")[1];
						$("groupIds_f").options.add(new Option(selable,selvalue));
				}
			}
	}
	function efind(){
		$("tabSwTable").style.border=0;
		var gridObj = pmGrid;
		var braObj = pmBar;
		var url = conextPath+"/role/roleAction!roleUserList.action";
		if($("userOutRoleDiv").style.display==""){
			url = conextPath+"/role/roleAction!selingUerList.action";
			url+="?dto.role.roleId="+parent.pmGrid.getSelectedId()+"&dto.pageSize2=" + pageSize2;
			gridObj = pmGrid2;
			braObj = pmBar2;
		}else{
			url+="?dto.role.roleId="+parent.pmGrid.getSelectedId()+"&dto.pageSize=" + pageSize;
		}
		var ajaxRest = dhtmlxAjax.postSync(url,"findForm").xmlDoc.responseText;
	   	if(ajaxRest=="failed"){
	   		hintMsg("查询时发生错误");
	   		return;
	   	}
	   	var datas = ajaxRest.split("$");
    	gridObj.clearAll();
    	if(datas[1] != ""){
	    	jsons = eval("(" + datas[1] +")");
	    	gridObj.parse(jsons, "json");
	    	pageInfoBk();
	    	setPageNoSizeCount(datas[0],braObj);
	    	pageInfoRce();
    		if($("userOutRoleDiv").style.display=="")
    			setRowNum(gridObj,"","",pageNo2,pageSize2);
    		else
	   			setRowNum(gridObj);
	   		markerChk();
	   		pageArrowSet();
    		fW_ch.setModal(false);
    		fW_ch.hide();   	
    	}else{
	    	pageInfoBk();
    		setPageNoSizeCount(datas[0],braObj);
    		pageInfoRce();
    		markerChk();
    		pageArrowSet();
	    	hintMsg("没查到相关记录");
	    }
	}
	function markerChk(){
		if($("userOutRoleDiv").style.display==""&&selUserIds!=""){
			for(var i = 0; i <pmGrid2.getRowsNum(); i++){
				if(selUserIds.indexOf(pmGrid2.getRowId(i))>=0){
					pmGrid2.cells(pmGrid2.getRowId(i),0).setValue(true);
				}			
			}
		}else if(delUserIds!=""){
			for(var i = 0; i <pmGrid.getRowsNum(); i++){
				if(delUserIds.indexOf(pmGrid.getRowId(i))>=0){
					pmGrid.cells(pmGrid.getRowId(i),0).setValue(true);
				}			
			}		
		}
	}
	function pageInfoBk(){
    	if($("userOutRoleDiv").style.display==""){
    		inPageNo = pageNo;
    		inPageSize = pageSize;
    		intPageCount = pageCount;
    	}	
	}
	function pageInfoRce(){
		if($("userOutRoleDiv").style.display==""){
			pageNo2 = pageNo;
			pageSize2 = pageSize;
			pageCount2 = pageCount;
			pageNo = inPageNo;
			pageSize = inPageSize;
			pageCount = pageCount;
		}
	}
	function initW_ch(obj, divId, mode, w, h,wId){//初始化 window
		importWinJs();
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w,h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.setDimension(w,h);
			obj.show();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}	
	function initCufmsW(){//初始化window
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.vp.style.border = "#909090 1px solid";
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	
	function pageArrowSet(){
		if($("userOutRoleDiv").style.display==""){
			if(pageNo2 == 1){
				pmBar2.disableItem("first");
				pmBar2.disableItem("pervious");
				if(pageCount2 <= 1){
					pmBar2.disableItem("next");
					pmBar2.disableItem("last");
					pmBar2.disableItem("slider");
				}else{
					pmBar2.enableItem("next");
					pmBar2.enableItem("last");
					pmBar2.enableItem("slider");
				}
			}else if(pageNo2 == pageCount2){
				pmBar2.enableItem("first");
				pmBar2.enableItem("pervious");
				pmBar2.disableItem("next");
				pmBar2.disableItem("last");
			}else if(pageNo2 > 1 && pageNo2 < pageCount2){
				pmBar2.enableItem("first");
				pmBar2.enableItem("pervious");
				pmBar2.enableItem("next");
				pmBar2.enableItem("last");
			}else{
				pmBar2.disableItem("first");
				pmBar2.disableItem("pervious");
				pmBar2.disableItem("next");
				pmBar2.disableItem("last");
				pmBar2.disableItem("slider");
			}
		}	
	}
