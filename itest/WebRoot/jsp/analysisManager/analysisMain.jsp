﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="ww" uri="/webwork"%>
<HTML>
<HEAD>
	<TITLE>MYPM项目管理平台</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/layout/codebase/dhtmlxlayout.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/layout/codebase/skins/dhtmlxlayout_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/layout/codebase/dhtmlxlayout.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
	<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
</HEAD>
<BODY style="overflow-y:hidden;overflow-x:hidden;">
   <ww:hidden id="repTree" name="analysisDto.treeStr"></ww:hidden>
    <ww:hidden id="canView" name="analysisDto.canView"></ww:hidden>
	<table cellspacing="0" cellpadding="0" align="center" border="0">
		<tr><td><div id="analysisMainDiv" style="width:600px; height:590px"></div></td></tr>
	</table>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
</BODY>
<script type="text/javascript">
	var projectSelectW_ch, parameterW_ch;
	$("analysisMainDiv").style.width = clientWidth - 12;
	$("analysisMainDiv").style.height = document.body.clientHeight - 20;
	var mypmLayout = new dhtmlXLayoutObject("analysisMainDiv", "2U");
	var views = mypmLayout.listViews();
	mypmLayout.items[0].setText("分析度量");
	mypmLayout.items[0].setWidth(220);
	mypmLayout.items[1].hideHeader();
	var mypmTree = mypmLayout.items[0].attachTree(0);
	mypmTree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	mypmTree.enableHighlighting(1);
	mypmTree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	mypmTree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	mypmTree.setStdImages("book.gif","books_open.gif","books_close.gif");
	mypmTree.setOnClickHandler(onclickHdl);
	var treeNodesInfo = $("repTree").value;
	var treeNodeArray = treeNodesInfo.split(";");
	var rootNodeId ="";
	for(var i=0;i<treeNodeArray.length-1;i++){
		var nodeContext= treeNodeArray[i];
		var node = nodeContext.split(",");
		var parentId =node[0] ;
		var id  = node[1];
		var text = node[2];
		if(text=="日提交BUG趋势"){
			text="日提交|关闭BUG趋势"
		}
		mypmTree.insertNewItem(parentId,id,text);
		mypmTree.setUserData(node[1],"myUrl",node[3]);
		if(i==0)
			rootNodeId = node[1];

	}
	//if(treeNodeArray.length>2){
	//	mypmTree.insertNewItem('143','227','简要统计');
	//	mypmTree.setUserData('227',"myUrl",'/singleTestTask/singleTestTaskAction!swTestTask4Report.action?dto.repTemplet=bugSummary_750_540_bugDate');
	//}
	var intNodeId ="",initUrl="",paraWinObj;
	function onclickHdl(id){
		if($("canView").value=="0"){
			hintMsg("免费版度量分析只可查看1000次,2000元便可购买",200);
			return;
		}
		var url =  mypmTree.getUserData(id,"myUrl");
		if(url == "" || url == "bl"||url.indexOf("goAnalysisMain.action")>0)return;
		var urlArr = url.split("_");
		if(urlArr.length==1){//不需要输参数
			mypmLayout.items[1].attachURL(conextPath + url);
			return;
		}
		if(id == intNodeId){
			if(typeof parameterW_ch != "undefined" && parameterW_ch.isHidden())
				parameterW_ch.show();//在测试统计报表中,调整了报表参数窗口的位置,这里要置回去
				if(url.indexOf("singleTestTaskAction")<0)
					parameterW_ch.centerOnScreen();
		}else{
			parameterW_ch = initWch(parameterW_ch, "", false, urlArr[1], urlArr[2], "parameterW_ch", "");
			parameterW_ch.setDimension(urlArr[1], urlArr[2]);
			parameterW_ch.setText(mypmTree.getItemText(id));
			if(url.indexOf("singleTestTaskAction")<0)
				parameterW_ch.centerOnScreen();
			url = urlArr[0];
			if(typeof urlArr[3] != "undefined"&&url.indexOf("singleTestTaskAction")<0){//测试那块不走报表BLH所以要区分一下
				url += "&analysisDto.parameter=" + urlArr[3];
			}
			if(urlArr.length==4&&url.indexOf("singleTestTaskAction")>0){
				url += "&dto.dateChk=" + urlArr[3];
			}
			//处理测试报表,加这些代码是不用每次attrchUrl 直接把相关值设置到参数页面中 要求在参数页面中,把自身的windows 设置为父窗口的paraWinObj变量上
			if(initUrl.indexOf("singleTestTaskAction")>0&&url.indexOf("singleTestTaskAction")>0&&typeof parameterW_ch !="undefined"){
				if(window.screen.width>=1152)
					parameterW_ch.setPosition(355, 20);
				else
					parameterW_ch.setPosition(240, 20);				
				if(urlArr.length==4){
					paraWinObj.$("repDiv").style.display=""; 
					paraWinObj.$("dateChk").value =urlArr[3];
				}else{
					paraWinObj.$("repDiv").style.display="none"; 
					paraWinObj.$("dateChk").value ="";
				}
				var templetId = urlArr[0].split("=")[1];
				paraWinObj.$("repTemplet").value=templetId;
				if("bugModuleDistbuStat"!=templetId && typeof paraWinObj.moduleTreeW_ch!="undefined")
					paraWinObj.moduleTreeW_ch.hide();
				var taskId = paraWinObj.pmGrid.getSelectedId();
				if(taskId!="")
					paraWinObj.pmGrid.cells(taskId, 0).setValue(false);
				parameterW_ch.show();
				parameterW_ch.bringToTop();
				parameterW_ch.setModal(false);
				intNodeId = id;
				return;
			}
			parameterW_ch.attachURL(conextPath + url);
			intNodeId = id;
			initUrl = url;
		}
	}
	openInitMypmTree();
	function openInitMypmTree(){
		mypmTree.closeAllItems(rootNodeId);
		mypmTree.openItem(rootNodeId);
		var subItemIdsArr = mypmTree.getSubItems(rootNodeId).split(",");
		for(var i=0; i<subItemIdsArr.length; i++){
			mypmTree.openItem(subItemIdsArr[i]);
		}
	}
	//hintMsg("mypm2.2 for mysql版,不支持测试度量功能,在线体验环境为oralce版本支持该功能,2.2版本己内置报表引擎，近期推出的2.3for mysql中实现",750);
</script>
</html>