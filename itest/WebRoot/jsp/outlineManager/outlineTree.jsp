<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<html>
	<head>
	<script type="text/javascript">
		var custContextMenuFlg=0;
	</script>
		<title>测试大刚</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/ext/dhtmlxtree_ed.js"></script>
	    <script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	    <link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<body bgcolor="#ffffff" leftmargin="0" topmargin="3" marginwidth="0" oncontextmenu="return false" style="overflow-y:hidden;">
	<ww:form theme="simple" method="post" id="updForm" name="updForm" namespace="/outLineManager" action="">
	<ww:hidden id="dtoCurrNodeId" name="dto.currNodeId"></ww:hidden>
	<ww:hidden id="dtoNodeName" name="dto.nodeName"></ww:hidden>
	<ww:hidden id="dtoParentNodeId" name="dto.parentNodeId"></ww:hidden>
	</ww:form>
	<ww:hidden id="taskId" name="dto.taskId"></ww:hidden>
	<ww:hidden id="isCommit" name="dto.isCommit"></ww:hidden>
	<input type="hidden" name="nodeDataStr" value="${nodeDataStr}" id="nodeDataStr"/>
	<input type="hidden" name="delNodeId" value="" id="delNodeId"/>
	<input type="hidden" id="editNodeName" name="editNodeName"/>
	<input type="hidden" id="editNodeId" name="editNodeId"/>
	<div id="treeBox" style="width:100%;height:100%"></div> 
	<script type="text/javascript">
		popContextMenuFlg=0;
		document.oncontextmenu=function(){return   false};
		 <pmTag:priviChk urls="outLineAction!updateNode,outLineAction!deleteNode,outLineAction!move" varNames="canUp,canDel,canMv"/>
		 if("${dto.isCommit}"=="1")//把提交测试置灰
		 	try{
		 		parent.mypmLayout.items[1]._frame.contentWindow.pmBar.disableItem("subMod");
		 	}catch(err){}
		 else
		 	try{
		 		parent.mypmLayout.items[1]._frame.contentWindow.pmBar.enableItem("subMod");
		 	}catch(err){}
		 if($("nodeDataStr").value=="0,1,无数据,0,1"){
		 	canUp = false;
		 	canMv = false;
		 	canDel = false;
		 }
		 if(!canMv){
		 	parent.mypmLayout.items[1]._frame.contentWindow.pmBar.disableItem("mup");
		 	parent.mypmLayout.items[1]._frame.contentWindow.pmBar.disableItem("mdown");
		 }
	</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/outlineManager/outTree.js"></script>
	<script type="text/javascript">
	 //parent.tree = tree;
	 parent.mypmLayout.items[1]._frame.contentWindow.tree= tree;
	 //parent.treeWin = window;
	 parent.mypmLayout.items[1]._frame.contentWindow.treeWin = window;
	</script>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>	
	</body>
</html>
