	function initGrid(GridObj,dataId,BarObj,noSetRowNum){
		var data = $(dataId).value;
	    if(data != ""){
	    	data = data.replace(/[\r\n]/g, "");
		    var datas = data.split("$");
		    if(datas[1] != ""){
		    	GridObj.clearAll();
		    	jsons = eval("(" + datas[1] +")");
		    	GridObj.parse(jsons, "json");
		    	setPageNoSizeCount(datas[0],BarObj);
		    	if(typeof noSetRowNum == "undefined"){
		    		setRowNum(GridObj);
		    	}
		    }else{
		    	pmBar.disableItem("slider");
		    	setPageNoSizeCount(datas[0],BarObj);
		    }
	    }	
	}
    function setRowNum(GridObj,colNum,onePage,pageNov,pageSizev){
        var index = 1;
		if(typeof colNum != "undefined" && colNum!=""){
			index = colNum;
		}
		if(typeof onePage != "undefined"&&onePage!=""){
			for(var i = 0; i < GridObj.getRowsNum(); i++){
				GridObj.cells2(i,index).setValue((i + 1));
			}
		return ;		
		}
		var pageNoTmp = pageNo;
		var pageSizeTmp = pageSize;
		if(typeof pageNov != "undefined")
			pageNoTmp = pageNov;
		if(typeof pageSizev != "undefined")
			pageSizeTmp = pageSizev;
    	var rowIdsToDel ="";
		for(var i = 0; i < GridObj.getRowsNum(); i++){	
			GridObj.cells2(i,index).setValue((i + 1)+((parseInt(pageNoTmp)-1)*parseInt(pageSizeTmp)));
			if(i>(pageSizeTmp-1)){
				if(rowIdsToDel==""){
					rowIdsToDel = GridObj.getRowId(i);
				}else{
					rowIdsToDel = rowIdsToDel +"," +GridObj.getRowId(i);
				}
			}
		}
		if(rowIdsToDel !=""){
			var ids = rowIdsToDel.split(",");
			for(var l=0; l <ids.length; l++){
			 GridObj.deleteRow(ids[l]);
			}
		}
    }
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		return true;
	}
	

	